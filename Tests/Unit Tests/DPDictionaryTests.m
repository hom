//
//  DPDictionaryTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 22/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPDictionaryTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPDictionaryTests

//=============================================
// NOTE: Tests are executed from the bottom up
//=============================================

- (BOOL)testCollectEach {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1",
																	@"b", @"2",
																	@"c", @"3", nil];
	NSSet *set = [NSSet setWithObjects:@"1", @"2", @"3", nil];
	NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:@"a1", @"1",
																	  @"b2", @"2",
																	  @"c3", @"3", nil];
	
	return [[dict collect:MSG(stringByAppendingString:[set each])] isEqualToDictionary:result];
}

- (BOOL)testCollect {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1",
		@"b", @"2",
		@"c", @"3", nil];
	NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:@"A", @"1",
		@"B", @"2",
		@"C", @"3", nil];
	
	return [[dict collect:MSG(capitalizedString)] isEqualToDictionary:result];
}

//=================================================================
// NOTE: All select/find + each combinations are meaningless
// for dictionaries as the order of objects in a dictionary
// can not be predicted.
//=================================================================

// There's no need to fully test rejectWhere: as it uses the
// same implementation of selectWhere:
- (BOOL)testRejectWhere {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1",
		@"b", @"2",
		@"c", @"3", nil];
	NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:@"b", @"2", @"c", @"3", nil];
	
	return [[dict rejectWhere:MSG(hasPrefix:@"a"), nil] isEqualToDictionary:result];
}

- (BOOL)testSelectWhere_twoMessages {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1",
		@"b", @"2",
		@"abc", @"3", nil];
	NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1", @"abc", @"3", nil];
	
	return [[dict selectWhere:MSG(capitalizedString), MSG(hasPrefix:@"A"), nil] isEqualToDictionary:result];
}

- (BOOL)testSelectWhere_oneMessage {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1", @"b", @"2",
		@"abc", @"3", nil];
	NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1", @"abc", @"3", nil];
	
	return [[dict selectWhere:MSG(hasPrefix:@"a"), nil] isEqualToDictionary:result];
}

- (BOOL)testFindObjectWhere_twoMessages {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1", @"b", @"2",
		@"abc", @"3", nil];
	return [[dict findObjectWhere:MSG(capitalizedString), MSG(hasPrefix:@"B"), nil] isEqualToString:@"b"];
}

- (BOOL)testFindObjectWhere_oneMessage {
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"a", @"1", @"b", @"2",
		@"abc", @"3", nil];
	return [[dict findObjectWhere:MSG(hasPrefix:@"b"), nil] isEqualToString:@"b"];
}

@end
