//
//  DPCoroutineMultiThreadTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 03/08/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPCoroutineMultiThreadTests.h"
#import "DPCoroutine.h"


@implementation DPCoroutineMultiThreadTests

- (unsigned)numberOfIterations {
	return 50;
}

- (void)incByOne {
	while (OSAtomicIncrement32Barrier(&count) < 5) {
		DPCoroutineYield();
	}
}

- (void)checkCompletion {
	int32_t c = count;
	OSMemoryBarrier();
	
	while (c < 5) {
		DPCoroutineYield();
		OSMemoryBarrier();
		c = count;
	}
	
	[pool release];
	[self setStatus:YES forPendingTest:@"testMultiThreaded"];
}

- (void)generateJobs {
	[pool sendCoroutineMessage:MSG(incByOne)
							to:self];
	[pool spawnThread];
	[pool spawnThread];
	
	[pool sendCoroutineMessage:MSG(checkCompletion)
							to:self];
	
}

- (void)testMultiThreaded {
	count = 0;
	pool = [[DPThreadPool alloc] init];
	
	// Let UITestingKit mark us as a pending test.
	// Yes, its a bug but I'm not feeling like fixing it right now.
	[self receiveMessage:MSG(generateJobs)
			  afterDelay:0.1];
}

@end
