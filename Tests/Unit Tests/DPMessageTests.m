//
//  DPMessageTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 17/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPMessageTests.h"
#import "DPMessage.h"


@implementation DP1MessageTests

//=============================================
// NOTE: Tests are executed from the bottom up
//=============================================

/*- (BOOL)yes {
	return YES;
}

- (BOOL)testBuiltinApply {
	DPMessage *m = MSG(yes);
	Method meth = class_getInstanceMethod([self class], @selector(yes));
	dp_marg_list args = [m gccArgumentsFrame];
	//*((id *)args) = self;
	void *r = __builtin_apply(meth->method_imp, args, [m sizeOfArguments]);
	__builtin_return(r);
}*/

- (BOOL)testMessageLogging {
	NSLog(@"hasPrefix message = %@", MSG(hasPrefix:@"a"));
	return YES;
}

- (BOOL)testMessageWithSelector_Frame {
	dp_marg_list frame = dp_marg_malloc(sizeof(id) * 2 + sizeof(SEL));
	dp_margSetObject(frame, 0, id, self);
	dp_margSetObject(frame, sizeof(id), SEL, @selector(hasPrefix:));
	dp_margSetObject(frame, sizeof(id) + sizeof(SEL), id, @"a");
	DPMessage *m = [DPMessage messageWithSelector:@selector(hasPrefix:)
											frame:frame];
	Method meth = class_getInstanceMethod([NSString class], @selector(hasPrefix:));
	id firstArg = dp_margGetObject([m arguments], sizeof(id) + sizeof(SEL), id);
	
	DPAssert(m != nil, @"nil message returned!");
	DPAssert([m selector] == @selector(hasPrefix:), @"Wrong selector for the returned message.");
	DPAssert([m sizeOfArguments] >= method_getSizeOfArguments(meth), @"Too small arguments size");
	DPAssert(firstArg== @"a", @"Wrong argument in frame."
			 "Expected %@ <%p> and found %@ <%p>", @"a", @"a", firstArg, firstArg);
	return YES;
}

- (BOOL)testAllocaFallback {
	int i;
	unsigned int argsSize = method_getSizeOfArguments(class_getInstanceMethod([NSString class],
																			  @selector(hasPrefix:)));
	DPMessage *lastMsg = nil;
	
	for (i = 0; i < 100; i++) {
		DPMessage *m = MSG(hasPrefix:@"a");
		
		DPAssert(m != nil, @"nil message returned!");
		DPAssert(m != lastMsg, @"The new message is the same as the last message.");
		DPAssert([m selector] == @selector(hasPrefix:), @"Wrong selector for the returned message.");
		DPAssert([m sizeOfArguments] >= argsSize, @"Too small arguments size");
		
		lastMsg = m;
	}
	
	return YES;
}

- (BOOL)testDoubleMSG {
	DPMessage *m1 = MSG(stringWithCString:"hello");
	DPMessage *m2 = MSG(hasPrefix:@"h");
	NSString *s = [m1 sendTo:[NSString class]];
	return [m2 sendTo:s] != nil;
}

- (BOOL)testMessageCreation {
	DPMessage *m = MSG(hasPrefix:@"a");
	Method meth = class_getInstanceMethod([NSString class], @selector(hasPrefix:));
	id firstArg = dp_margGetObject([m arguments], sizeof(id) + sizeof(SEL), id);
	
	DPAssert(m != nil, @"nil message returned!");
	DPAssert([m selector] == @selector(hasPrefix:), @"Wrong selector for the returned message.");
	DPAssert([m sizeOfArguments] >= method_getSizeOfArguments(meth), @"Too small arguments size");
	DPAssert(firstArg== @"a", @"Wrong argument in frame."
			 "Expected %@ <%p> and found %@ <%p>", @"a", @"a", firstArg, firstArg);
	return YES;
}

@end
