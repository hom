//
//  DPNSObjectTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 27/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPNSObjectTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPNSObjectTests

- (NSString *)append:(NSString *)str {
	return [@"str" stringByAppendingString:str];
}

- (BOOL)testCollectEach {
	NSArray *arr = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	NSArray *r = [self collect:MSG(append:[arr each])];
	return [r isEqualToArray:[NSArray arrayWithObjects:@"str1", @"str2", @"str3", nil]];
}

- (BOOL)testReceive {
	NSArray *arr = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	NSMutableArray *result = [NSMutableArray array];
	
	[result receive:MSG(addObject:[arr each])];
	return [result isEqualToArray:arr];
}

- (NSString *)str {
	return @"str";
}

- (BOOL)testIfResponds {
	DPAssert([[self ifResponds:MSG(str)] isEqualToString:@"str"], @"Wrong return value");
	return [self ifResponds:MSG(addObject:@"a")] == nil;
}

@end
