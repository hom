//
//  DPCoroutineMultiThreadTests.h
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 03/08/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import <UITestingKit/UITestingKit.h>


@class DPThreadPool;

@interface DPCoroutineMultiThreadTests : DPTestCase {
	int32_t count;
	DPThreadPool *pool;
}

@end
