//
//  DPNSStringHOMSupport.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 26/07/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPNSStringHOMSupport.h"
#import "DPCollectionEnumeration.h"


@implementation DPNSStringHOMSupport

- (BOOL)testStringWithFormat {
	NSArray *arr = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	NSArray *results = [NSArray arrayWithObjects:@"1 a1", @"1 a2", @"1 a3", nil];
	NSArray *strings = [NSString collect:MSG(stringWithFormat:@"%d a%@", 1, [arr each])];
	return [strings isEqualToArray:results];
}

- (BOOL)testAppendFormat {
	NSMutableString *str = [NSMutableString string];
	NSArray *arr = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	[str receive:MSG(appendFormat:@"%@,", [arr each])];
	return [str isEqualToString:@"1,2,3,"];
}

@end
