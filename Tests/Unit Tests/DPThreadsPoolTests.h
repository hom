//
//  DPThreadsPoolTests.h
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 29/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import <UITestingKit/UITestingKit.h>
#import "DPMultiThreading.h"


@interface DPThreadsPoolTests : DPTestCase {
	int32_t count;
	DPThreadPool *pool;
}

@end
