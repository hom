//
//  DPSetTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 21/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPSetTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPSetTests

//=============================================
// NOTE: Tests are executed from the bottom up
//=============================================

- (BOOL)testCollectEach {
	NSSet *set1 = [NSSet setWithObjects:@"a", @"b", @"c", nil];
	NSSet *set2 = [NSSet setWithObjects:@"1", @"2", @"3", nil];
	NSSet *result = [NSSet setWithObjects:@"a1", @"b2", @"c3", nil];
	
	return [[set1 collect:MSG(stringByAppendingString:[set2 each])] isEqualToSet:result];
}

- (BOOL)testCollect {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	NSSet *result = [NSSet setWithObjects:@"A", @"B", @"Abc", nil];
	
	return [[set collect:MSG(capitalizedString)] isEqualToSet:result];
}

//=================================================================
// NOTE: All select/find + each combinations are meaningless
// for sets as the order of objects in a set can not be predicted.
//=================================================================

// There's no need to fully test rejectWhere: as it uses the
// same implementation of selectWhere:
- (BOOL)testRejectWhere {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	NSSet *result = [NSSet setWithObject:@"b"];
	
	return [[set rejectWhere:MSG(hasPrefix:@"a"), nil] isEqualToSet:result];
}

- (BOOL)testSelectWhere_twoMessages {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	NSSet *result = [NSSet setWithObjects:@"a", @"abc", nil];
	
	return [[set selectWhere:MSG(capitalizedString), MSG(hasPrefix:@"A"), nil] isEqualToSet:result];
}

- (BOOL)testSelectWhere_oneMessage {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	NSSet *result = [NSSet setWithObjects:@"a", @"abc", nil];
	
	return [[set selectWhere:MSG(hasPrefix:@"a"), nil] isEqualToSet:result];
}

- (BOOL)testFindObjectWhere_twoMessages {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	return [[set findObjectWhere:MSG(capitalizedString), MSG(hasPrefix:@"B"), nil] isEqualToString:@"b"];
}

- (BOOL)testFindObjectWhere_oneMessage {
	NSSet *set = [NSSet setWithObjects:@"a", @"b", @"abc", nil];
	return [[set findObjectWhere:MSG(hasPrefix:@"b"), nil] isEqualToString:@"b"];
}

@end
