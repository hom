//
//  DPThreadsPoolTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 29/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPThreadsPoolTests.h"
#include <libkern/OSAtomic.h>
#include <sched.h>


@implementation DPThreadsPoolTests

- (id)init {
	if ((self = [super init]))
		count = 0;
	
	return self;
}

- (unsigned)numberOfIterations {
	return 50;
}

- (BOOL)verifiesAllTests {
	return YES;
}

- (void)incrementCounter {
	// Make sure the other threads also gets a chance to wake up
	sched_yield();
	if (OSAtomicIncrement32(&count) == 3) {
		[pool release]; pool = nil;
		[self setStatus:YES forPendingTest:@"testSimplePool"];
	}
}

- (void)testSimplePool {
	count = 0;
	pool = [[DPThreadPool poolWithNumberOfThreads:3] retain];
	[pool sendMessage:MSG(incrementCounter) to:self];
	[pool sendMessage:MSG(incrementCounter) to:self];
	[pool sendMessage:MSG(incrementCounter) to:self];
}

@end
