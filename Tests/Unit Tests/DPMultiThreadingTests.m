//
//  DPMultiThreadingTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 26/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPMultiThreadingTests.h"
#import "DPMultiThreading.h"


@implementation DPMultiThreadingTests

- (unsigned)numberOfIterations {
	return 50;
}

- (BOOL)verifiesAllTests {
	return YES;
}

- (id)threadMethod {
	//sleep(3);
	return @"hello";
}

- (BOOL)testNSThreadSendTo {
	// Launch a new worker thread
	NSThread *thread = [NSThread detachNewWorkerThread];
	
	// Make sure it's a worker thread
	DPAssert([thread defaultInvocationQueue] != nil, @"Worker thread (%@) has"
			 " no invocation queue", thread);
	
	//CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
	
	// Give our thread something to do and keep the future around
	NSString *str = [thread sendMessage:MSG(threadMethod)
									 to:self];
	
	// Be sure it's a future
	//DPAssert(DPObjectIsFuture(str), @"The returned object is not a future");
	
	// Ignoring the fact it's a future, let's see if it matches our expectations
	// of a string. Since our thread waits 3 seconds before it returns us the
	// string this blocks us during that period.
	DPAssert([str isEqualToString:@"hello"], @"Wrong string result");
	
	DPAssert([str self] == @"hello", @"Wrong constant string instance");
	
	// Make sure we blocked for at least 3 seconds
	//DPAssert(CFAbsoluteTimeGetCurrent() - t > 3.0, @"Didn't block long enough");
	
	// Did the future really mutated as expected?
	DPAssert(!DPObjectIsFuture(str), @"The future didn't mutate even though it got a value");
	
	// Let our thread go
	[thread terminate];
	
	return YES;
}

- (BOOL)testNSObjectFuture {
	//CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
	NSString *str = [self future:MSG(threadMethod)];
	
	// Be sure it's a future
	//DPAssert(DPObjectIsFuture(str), @"The returned object is not a future");
	
	// Ignoring the fact it's a future, let's see if it matches our expectations
	// of a string. Since our thread waits 3 seconds before it returns us the
	// string this blocks us during that period.
	DPAssert([str isEqualToString:@"hello"], @"Wrong string result");
	
	DPAssert([str self] == @"hello", @"Wrong constant string instance");
	
	// Make sure we blocked for at least 3 seconds
	//DPAssert(CFAbsoluteTimeGetCurrent() - t > 3.0, @"Didn't block long enough");
	
	// Did the future really mutated as expected?
	DPAssert(!DPObjectIsFuture(str), @"The future didn't mutate even though it got a value");
	return YES;
}

@end
