//
//  DPCoroutineSingleThreadTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 02/08/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPCoroutineSingleThreadTests.h"
#import "DPCoroutine.h"


@implementation DPCoroutineSingleThreadTests

- (unsigned)numberOfIterations {
	return 20;
}

- (void)incByTwo {
	while (count < 10) {
		count += 2;
		DPCoroutineYield();
	}
}

- (void)decByOne {
	while (count < 10) {
		--count;
		DPCoroutineYield();
	}
}

- (void)finishSingleThreadTest {
	while (count < 10)
		DPCoroutineYield();
	
	[self setStatus:YES forPendingTest:@"testSingleThread"];
}

- (void)testSingleThread {
	count = 0;
	[self coroutine:MSG(finishSingleThreadTest)];
	[self coroutine:MSG(incByTwo)];
	[self coroutine:MSG(decByOne)];
}

@end
