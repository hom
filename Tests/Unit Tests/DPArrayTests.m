//
//  DPArrayTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 18/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPArrayTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPArrayTests

//=============================================
// NOTE: Tests are executed from the bottom up
//=============================================

- (BOOL)testCollectEach {
	NSArray *arr1 = [NSArray arrayWithObjects:@"a", @"b", @"c", nil];
	NSArray *arr2 = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	NSArray *result = [NSArray arrayWithObjects:@"a1", @"b2", @"c3", nil];
	
	// -collect looks for "enumerated arguments" (the result of -each) in the passed message.
	// If found, every iteration on the passed message will include the next object of the
	// enumerated argument. In our case, every object in arr1 will receive a stringByAppendingString:
	// message with the matching object in arr2.
	return [[arr1 collect:MSG(stringByAppendingString:[arr2 each])] isEqualToArray:result];
}

- (BOOL)testCollect {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *result = [NSArray arrayWithObjects:@"A", @"B", @"Abc", nil];
	
	return [[arr collect:MSG(capitalizedString)] isEqualToArray:result];
}

- (BOOL)testCollect_20Objects {
	NSMutableArray *arr = [NSMutableArray array];
	NSMutableArray *result = [NSMutableArray array];
	int i;
	for (i = 0; i < 20; i++) {
		[arr addObject:[NSString stringWithFormat:@"%d", i]];
		[result addObject:[NSString stringWithFormat:@"%da", i]];
	}
	
	return [[arr collect:MSG(stringByAppendingString:@"a")] isEqualToArray:result];
}

// There's no need to fully test rejectWhere: as it uses the
// same implementation of selectWhere:
- (BOOL)testRejectWhere {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *result = [NSArray arrayWithObject:@"b"];
	
	return [[arr rejectWhere:MSG(hasPrefix:@"a"), nil] isEqualToArray:result];
}

- (BOOL)testSelectWhereEach_twoMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *suffixes = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	NSArray *result = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	
	return [[arr selectWhere:MSG(stringByAppendingString:[suffixes each]),
							 MSG(hasPrefix:[prefixes each]), nil] isEqualToArray:result];
}

- (BOOL)testSelectWhere_twoMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *result = [NSArray arrayWithObjects:@"a", @"abc", nil];
	
	return [[arr selectWhere:MSG(capitalizedString), MSG(hasPrefix:@"A"), nil] isEqualToArray:result];
}

- (BOOL)testSelectWhereEach_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	NSArray *result = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	
	return [[arr selectWhere:MSG(hasPrefix:[prefixes each]), nil] isEqualToArray:result];
}

- (BOOL)testSelectWhere_oneMessage_30Objects {
	NSMutableArray *arr = [NSMutableArray array];
	NSMutableArray *result = [NSMutableArray array];
	int i;
	for (i = 0; i < 30; i++) {
		[arr addObject:[NSString stringWithFormat:@"%d", i]];
		if (i % 10 == 1) {
			[result addObject:[NSString stringWithFormat:@"%d", i]];
		}
	}
	
	return [[arr selectWhere:MSG(hasSuffix:@"1"), nil] isEqualToArray:result];
}

- (BOOL)testSelectWhere_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *result = [NSArray arrayWithObjects:@"a", @"abc", nil];
	
	return [[arr selectWhere:MSG(hasPrefix:@"a"), nil] isEqualToArray:result];
}

- (BOOL)testFindObjectWhereEach_threeMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *suffixes = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"B", @"A", @"A", nil];
	
	return [[arr findObjectWhere:MSG(stringByAppendingString:[suffixes each]),
								   MSG(capitalizedString), MSG(hasPrefix:[prefixes each]),
								   nil] isEqualToString:@"abc"];
}

- (BOOL)testFindObjectWhere_twoMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	return [[arr findObjectWhere:MSG(capitalizedString), MSG(hasPrefix:@"B"), nil] isEqualToString:@"b"];
}

- (BOOL)testFindObjectWhereEach_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"b", @"a", @"a", nil];
	
	return [[arr findObjectWhere:MSG(hasPrefix:[prefixes each]), nil] isEqualToString:@"abc"];
}

- (BOOL)testFindObjectWhere_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"abc", nil];
	return [[arr findObjectWhere:MSG(hasPrefix:@"b"), nil] isEqualToString:@"b"];
}

@end
