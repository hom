//
//  DPEnumeratorTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 25/07/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPEnumeratorTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPEnumeratorTests

- (BOOL)testIsEnumerator {
	return DPIsEnumeratedArgument([[NSArray array] each]);
}

- (BOOL)testIsNotEnumerator {
	intptr_t i = 12365;
	return !DPIsEnumeratedArgument((void *)i);
}

@end
