//
//  DPSmallArraySpeedTests.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 19/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPSmallArraySpeedTests.h"
#import "DPCollectionEnumeration.h"


@implementation DPSmallArraySpeedTests

- (id)init {
	if ((self = [super init])) {
		smallArray = [[NSArray alloc] initWithObjects:@"a", @"b", @"ca", nil];
		smallArray2 = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", nil];
	}
	
	return self;
}

- (void)dealloc {
	[smallArray release]; smallArray = nil;
	[smallArray2 release]; smallArray2 = nil;
	[super dealloc];
}

- (unsigned)numberOfIterations {
	return 1000000;
}

- (id)testCollect {
	return [smallArray collect:MSG(capitalizedString)];
}

- (BOOL)verifyTestCollect:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"A", @"B", @"Ca", nil]];
}

- (id)testCollectNSEnumerator {
	NSMutableArray *arr = [NSMutableArray array];
	NSEnumerator *e = [smallArray objectEnumerator];
	NSString *str;
	
	while ((str = [e nextObject]))
		[arr addObject:[str capitalizedString]];
	
	return arr;
}

- (BOOL)verifyTestCollectNSEnumerator:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"A", @"B", @"Ca", nil]];
}

- (id)testCollectEach {
	return [smallArray collect:MSG(stringByAppendingString:[smallArray2 each])];
}

- (BOOL)verifyTestCollectEach:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a1", @"b2", @"ca3", nil]];
}

- (id)testCollectEachObjectAtIndex {
	NSMutableArray *arr = [NSMutableArray array];
	unsigned i;
	
	for (i = 0; i < [smallArray count]; i++)
		[arr addObject:[[smallArray objectAtIndex:i] stringByAppendingString:[smallArray2 objectAtIndex:i]]];
	
	return arr;
}

- (BOOL)verifyTestCollectEachObjectAtIndex:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a1", @"b2", @"ca3", nil]];
}

- (id)testCollectEachNSEnumerator {
	NSEnumerator *e1 = [smallArray objectEnumerator];
	NSEnumerator *e2 = [smallArray2 objectEnumerator];
	NSMutableArray *results = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e1 nextObject]))
		[results addObject:[str stringByAppendingString:[e2 nextObject]]];
	
	return results;
}

- (BOOL)verifyTestCollectEachNSEnumerator:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a1", @"b2", @"ca3", nil]];
}

#pragma mark -

- (id)testFindObjectNSEnumeratorEach_threeMessages {
	NSArray *prefixes = [NSArray arrayWithObjects:@"B", @"A", @"C", nil];
	NSEnumerator *e1 = [smallArray objectEnumerator];
	NSEnumerator *e2 = [smallArray2 objectEnumerator];
	NSEnumerator *e3 = [prefixes objectEnumerator];
	NSString *str;
	
	while ((str = [e1 nextObject])) {
		if ([[[str stringByAppendingString:[e2 nextObject]] capitalizedString] hasPrefix:[e3 nextObject]])
			return str;
	}
	
	return nil;
}

- (BOOL)verifyTestFindObjectNSEnumeratorEach_threeMessages:(NSString *)str {
	return [str isEqualToString:@"ca"];
}

- (id)testFindObjectWhereEach_threeMessages {
	NSArray *prefixes = [NSArray arrayWithObjects:@"B", @"A", @"C", nil];
	
	return [smallArray findObjectWhere:MSG(stringByAppendingString:[smallArray2 each]),
								MSG(capitalizedString), MSG(hasPrefix:[prefixes each]),
								nil];
}

- (BOOL)verifyTestFindObjectWhereEach_threeMessages:(NSString *)str {
	return [str isEqualToString:@"ca"];
}

// --

- (id)testFindObjectNSEnumerator_twoMessages {
	NSEnumerator *e = [smallArray objectEnumerator];
	NSString *str;
	
	while ((str = [e nextObject])) {
		if ([[str capitalizedString] hasPrefix:@"B"])
			return str;
	}
	
	return nil;
}

- (BOOL)verifyTestFindObjectNSEnumerator_twoMessages:(NSString *)str {
	return [str isEqualToString:@"b"];
}

- (id)testFindObjectWhere_twoMessages {
	return [smallArray findObjectWhere:MSG(capitalizedString), MSG(hasPrefix:@"B"), nil];
}

- (BOOL)verifyTestFindObjectWhere_twoMessages:(NSString *)str {
	return [str isEqualToString:@"b"];
}

// ---

- (id)testFindObjectNSEnumeratorEach_oneMessage {
	NSArray *prefixes = [NSArray arrayWithObjects:@"b", @"a", @"c", nil];
	NSEnumerator *e1 = [smallArray objectEnumerator];
	NSEnumerator *e2 = [prefixes objectEnumerator];
	NSString *str;
	
	while ((str = [e1 nextObject])) {
		if ([str hasPrefix:[e2 nextObject]])
			return str;
	}
	
	return nil;
}

- (BOOL)verifyTestFindObjectNSEnumeratorEach_oneMessage:(NSString *)str {
	return [str isEqualToString:@"ca"];
}

- (id)testFindObjectWhereEach_oneMessage {
	NSArray *prefixes = [NSArray arrayWithObjects:@"b", @"a", @"c", nil];
	return [smallArray findObjectWhere:MSG(hasPrefix:[prefixes each]), nil];
}

- (BOOL)verifyTestFindObjectWhereEach_oneMessage:(NSString *)str {
	return [str isEqualToString:@"ca"];
}

// ----

- (id)testFindObjectNSEnumerator_oneMessage {
	NSEnumerator *e = [smallArray objectEnumerator];
	NSString *str;
	
	while ((str = [e nextObject])) {
		if ([str hasPrefix:@"b"])
			return str;
	}
	
	return nil;
}

- (BOOL)verifyTestFindObjectNSEnumerator_oneMessage:(NSString *)str {
	return [str isEqualToString:@"b"];
}

- (id)testFindObjectWhere_oneMessage {
	return [smallArray findObjectWhere:MSG(hasPrefix:@"b"), nil];
}

- (BOOL)verifyTestFindObjectWhere_oneMessage:(NSString *)str {
	return [str isEqualToString:@"b"];
}

#pragma mark -

- (id)testSelectWhereEachNSEnumerator_twoMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *suffixes = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	NSEnumerator *e1 = [arr objectEnumerator];
	NSEnumerator *e2 = [suffixes objectEnumerator];
	NSEnumerator *e3 = [prefixes objectEnumerator];
	NSMutableArray *result = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e1 nextObject]))
		if ([[str stringByAppendingString:[e2 nextObject]] hasPrefix:[e3 nextObject]])
			[result addObject:str];
	
	return result;
}

- (BOOL)verifyTestSelectWhereEachNSEnumerator_twoMessages:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", @"b", @"abc", nil]];
}

- (id)testSelectWhereEach_twoMessages {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *suffixes = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	
	return [arr selectWhere:MSG(stringByAppendingString:[suffixes each]),
							 MSG(hasPrefix:[prefixes each]), nil];
}

- (BOOL)verifyTestSelectWhereEach_twoMessages:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", @"b", @"abc", nil]];
}

// ---

- (id)testSelectWhereNSEnumerator_twoMessages {
	NSEnumerator *e = [smallArray objectEnumerator];
	NSMutableArray *arr = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e nextObject]))
		if ([[str capitalizedString] hasPrefix:@"A"])
			[arr addObject:str];
	
	return arr;
}

- (BOOL)verifyTestSelectWhereNSEnumerator_twoMessages:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObject:@"a"]];
}

- (id)testSelectWhere_twoMessages {
	return [smallArray selectWhere:MSG(capitalizedString), MSG(hasPrefix:@"A"), nil];
}

- (BOOL)verifyTestSelectWhere_twoMessages:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObject:@"a"]];
}

// ---

- (id)testSelectWhereEachNSEnumerator_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	NSEnumerator *e1 = [arr objectEnumerator];
	NSEnumerator *e2 = [prefixes objectEnumerator];
	NSMutableArray *result = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e1 nextObject]))
		if ([str hasPrefix:[e2 nextObject]])
			[result addObject:str];
	
	return result;
}

- (BOOL)verifyTestSelectWhereEachNSEnumerator_oneMessage:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", @"b", @"abc", nil]];
}

- (id)testSelectWhereEach_oneMessage {
	NSArray *arr = [NSArray arrayWithObjects:@"a", @"b", @"c", @"abc", nil];
	NSArray *prefixes = [NSArray arrayWithObjects:@"a", @"b", @"a", @"a", nil];
	
	return [arr selectWhere:MSG(hasPrefix:[prefixes each]), nil];
}

- (BOOL)verifyTestSelectWhereEach_oneMessage:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", @"b", @"abc", nil]];
}

// ---

- (id)testSelectWhereNSEnumerator_oneMessage_30Objects {
	NSMutableArray *arr = [NSMutableArray array];
	int i;
	for (i = 0; i < 30; i++)
		[arr addObject:[NSString stringWithFormat:@"%d", i]];
	
	NSEnumerator *e = [arr objectEnumerator];
	NSMutableArray *result = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e nextObject]))
		if ([str hasSuffix:@"1"])
			[result addObject:str];
	
	return result;
}

- (BOOL)verifyTestSelectWhereNSEnumerator_oneMessage_30Objects:(NSArray *)arr {
	NSMutableArray *result = [NSMutableArray array];
	int i;
	for (i = 0; i < 30; i++)
		if (i % 10 == 1)
			[result addObject:[NSString stringWithFormat:@"%d", i]];
	
	return [arr isEqualToArray:result];
}

- (id)testSelectWhere_oneMessage_30Objects {
	NSMutableArray *arr = [NSMutableArray array];
	int i;
	for (i = 0; i < 30; i++)
		[arr addObject:[NSString stringWithFormat:@"%d", i]];
	
	return [arr selectWhere:MSG(hasSuffix:@"1"), nil];
}

- (BOOL)verifyTestSelectWhere_oneMessage_30Objects:(NSArray *)arr {
	NSMutableArray *result = [NSMutableArray array];
	int i;
	for (i = 0; i < 30; i++)
		if (i % 10 == 1)
			[result addObject:[NSString stringWithFormat:@"%d", i]];
	
	return [arr isEqualToArray:result];
}

// ---

- (id)testSelectWhereNSEnumerator_oneMessage {
	NSEnumerator *e = [smallArray objectEnumerator];
	NSMutableArray *arr = [NSMutableArray array];
	NSString *str;
	
	while ((str = [e nextObject])) {
		if ([str hasPrefix:@"a"])
			[arr addObject:str];
	}
	
	return arr;
}

- (BOOL)verifyTestSelectWhereNSEnumerator_oneMessage:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", nil]];
}

- (id)testSelectWhere_oneMessage {
	return [smallArray selectWhere:MSG(hasPrefix:@"a"), nil];
}

- (BOOL)verifyTestSelectWhere_oneMessage:(NSArray *)arr {
	return [arr isEqualToArray:[NSArray arrayWithObjects:@"a", nil]];
}
	
@end
