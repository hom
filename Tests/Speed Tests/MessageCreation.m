//
//  MessageCreation.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 09/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "MessageCreation.h"
#import "Message.h"


@implementation MessageCreation

- (unsigned)numberOfIterations {
	return 100000;
}

- (BOOL)testCreation {
	Message *m = MSG(hasPrefix:@"a");
	return m != nil;
}

@end
