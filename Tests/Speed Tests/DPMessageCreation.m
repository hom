//
//  DPMessageCreation.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 09/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPMessageCreation.h"
#import <HigherOrderMessaging/DPMessage.h>


@implementation DPMessageCreation

- (unsigned)numberOfIterations {
	return 1000000;
	//return UINT_MAX;
}

- (BOOL)testMessageCreation {
	return MSG(hasPrefix:@"a") != nil;
}

- (void)doNothing {
}

- (BOOL)testNormalMessage {
	[self doNothing];
	return YES;
}

@end
