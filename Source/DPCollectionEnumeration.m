//
//  DPCollectionEnumeration.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 11/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. Neither the name of Ofri Wolfus nor the names of his contributors
//  may be used to endorse or promote products derived from this software
//  without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
//  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "DPCollectionEnumeration.h"
#import "DPMessage.h"
#import "DPObjCRuntime.h"
#include <stdarg.h>
#include <alloca.h>
#include <libkern/OSAtomic.h>


@interface NSObject (DPIntrospectionAdditions)

/*
 * NOTE: Selectors of variable arguments methods must also be
 * added to -[_DPUninitializedMessage forward::] or the arguments
 * will get cut off in the message instance.
 */

/*
 * The following two methods allow us to customize at runtime
 * the types description used with a given message/selector.
 * NSObject's implementation simply returns the result of
 * class_getInstance(/Class)Method(), but NSString overrides
 * them in order to support its format string messages.
 * Currently NSString is the only class that has a custom
 * implementation for these.
 */
- (const char *)typeEncodingForSelector:(SEL)sel;
- (const char *)typeEncodingForMessage:(DPMessage *)msg;

@end


@interface DPIdEnumerator : DPEnumerator {
}

- (id)initWithObjects:(id *)obj count:(unsigned)c freeWhenDone:(BOOL)flag;
@end

@implementation DPIdEnumerator

typedef struct {
	id *objects;
	unsigned count, index;
	BOOL freeWhenDone;
} DPIDEnumContent;

- (id)initWithObjects:(id *)obj count:(unsigned)c freeWhenDone:(BOOL)flag {
	if ((self = [super init])) {
		DPIDEnumContent *content = calloc(1, sizeof(DPIDEnumContent));
		_reserved = content;
		content->objects = obj;
		content->count = c;
		content->index = 0U;
		content->freeWhenDone = flag;
	}
	
	return self;
}

- (void)dealloc {
	if (((DPIDEnumContent *)_reserved)->freeWhenDone)
		free(((DPIDEnumContent *)_reserved)->objects);
	free(_reserved);
	[super dealloc];
}

- (id)nextObject {
	id o = nil;
	DPIDEnumContent *content = _reserved;
	
	if (content->index < content->count) {
		o = content->objects[content->index];
		content->index++;
	}
	
	return o;
}

- (BOOL)isValid {
	return ((DPIDEnumContent *)_reserved)->index < ((DPIDEnumContent *)_reserved)->count;
}

- (unsigned)count {
	return ((DPIDEnumContent *)_reserved)->count;
}

- (void)reset {
	((DPIDEnumContent *)_reserved)->index = 0U;
}

@end


@interface DPArrayEnumerator : DPEnumerator {
}

- (id)initWithArray:(NSArray *)arr;
@end

@implementation DPArrayEnumerator

typedef struct {
	NSArray *arr;
	unsigned count, index;
	IMP objectAtIndexImp;
} DPArrayEnumeratorContent;

- (id)initWithArray:(NSArray *)a {
	if ((self = [super init])) {
		DPArrayEnumeratorContent *content = calloc(1, sizeof(DPArrayEnumeratorContent));
		_reserved = content;
		content->arr = [a retain];
		content->count = [a count];
		content->index = 0U;
		content->objectAtIndexImp = [a methodForSelector:@selector(objectAtIndex:)];
	}
	
	return self;
}

- (void)dealloc {
	[((DPArrayEnumeratorContent *)_reserved)->arr release];
	free(_reserved);
	[super dealloc];
}

- (id)nextObject {
	id o = nil;
	DPArrayEnumeratorContent *content = _reserved;
	
	if (content->index < content->count) {
		o = content->objectAtIndexImp(content->arr, @selector(objectAtIndex:), content->index);
		++(content->index);
	}
	
	return o;
}

- (BOOL)isValid {
	return ((DPArrayEnumeratorContent *)_reserved)->index < ((DPArrayEnumeratorContent *)_reserved)->count;
}

- (unsigned)count {
	return ((DPArrayEnumeratorContent *)_reserved)->count;
}

- (void)reset {
	((DPArrayEnumeratorContent *)_reserved)->index = 0U;
}

@end

@interface DPCollectionEnumerator : DPEnumerator {
}

@end

@implementation DPCollectionEnumerator

typedef struct {
	id <DPEnumeration> collection;
	DPEnumerationState state;
	id buff[16];
	unsigned count, index;
} DPCollectionEnumeratorContent;

- (id)initWithCollection:(id <DPEnumeration>)c {
	if ((self = [super init])) {
		DPCollectionEnumeratorContent *content = calloc(1, sizeof(DPCollectionEnumeratorContent));
		_reserved = content;
		content->collection = [(id)c retain];
		content->state.state = 0;
		content->state.items = content->buff;
		content->state.info = NULL;
		content->state.release = NULL;
		content->index = 0;
		content->count = [content->collection enumerateWithState:&(content->state)
														 objects:content->buff
														   count:16];
	}
	
	return self;
}

- (void)dealloc {
	DPCollectionEnumeratorContent *content = _reserved;
	if (content->state.release)
		content->state.release(&(content->state));
	
	[(id)(content->collection) release];
	free(_reserved);
	[super dealloc];
}

- (BOOL)isValid {
	return ((DPCollectionEnumeratorContent *)_reserved)->count > 0;
}

- (unsigned)count {
	DPCollectionEnumeratorContent *content = _reserved;
	
	if ([(id)(content->collection) respondsToSelector:@selector(count)])
		return [(id)(content->collection) count];
	else
		return content->count;
}

- (void)reset {
	DPCollectionEnumeratorContent *content = _reserved;
	
	if (content->state.release)
		content->state.release(&(content->state));
	content->state.state = 0;
	content->state.items = content->buff;
	content->state.info = NULL;
	content->state.release = NULL;
	content->index = 0;
	content->count = [content->collection enumerateWithState:&(content->state)
													 objects:content->buff
													   count:16];
}

- (id)nextObject {
	DPCollectionEnumeratorContent *content = _reserved;
	
	if (content->count == 0) {
		if (content->state.release) {
			content->state.release(&(content->state));
			content->state.release = NULL;
		}
		
		return nil;
	}
	
	if (content->index < content->count)
		return content->state.items[content->index++];
	else {
		content->index = 0;
		content->count = [content->collection enumerateWithState:&(content->state)
														 objects:content->buff
														   count:16];
		return [self nextObject];
	}
}

@end


@implementation DPEnumerator

#if defined(__LP64__) || defined(__ppc64__) || defined(__i386__) || defined(__x86_64__)
#define AtomicCompareAndSwapBarrier OSAtomicCompareAndSwap64Barrier
#else
#define AtomicCompareAndSwapBarrier OSAtomicCompareAndSwap32Barrier
#endif

// Nobody should ever need more than 100 enumerators at once!
// If you do, you're doing something horribly horribly wrong.
#define DP_MAX_ENUMERATORS	100
// This is a block of DP_MAX_ENUMERATORS DPEnumerator instances,
// allocated one after the other. Every instance is actually
// struct { Class isa; void *_reserved; }.
// Zeroed isa members are freed instances that can be reused.
static DPEnumerator *_DPEnumeratorsPool = NULL;



+ (void)load {
	// Before we do anything, we must allocate a pool of instances.
	_DPEnumeratorsPool = calloc(DP_MAX_ENUMERATORS, class_getInstanceSize(self));
}

// Override the default allocation method
// and return an instance from our pool.
+ (id)allocWithZone:(NSZone *)zone {
	unsigned i;
	DPEnumerator *enumerator = NULL;
	
	for (i = 0; i < DP_MAX_ENUMERATORS; i++) {
		enumerator = _DPEnumeratorsPool + i;
		if (AtomicCompareAndSwapBarrier(0, (intptr_t)self,
										(void *)enumerator /* we cast to void here to avoid the warning */))
		{
			break;
		}
	}
	
	// Make sure we got an enumerator and not ran out of space
	NSAssert(__builtin_expect(i < DP_MAX_ENUMERATORS, 1), @"Dude, you're using waay too much enumerators at once!");
	
	return enumerator;
}

// We never really free enumerators. We just reuse "released" ones.
- (void)dealloc {
	// Mark ourself as free
	self->isa = NULL;
	// Stop GCC from whining
	return;
	[super dealloc];
}

+ (id)enumeratorWithObjects:(id *)objects
					  count:(unsigned)count
			   freeWhenDone:(BOOL)flag
{
	return [[[DPIdEnumerator alloc] initWithObjects:objects
											  count:count
									   freeWhenDone:flag] autorelease];
}

+ (id)enumeratorForArray:(NSArray *)arr {
	return [[[DPArrayEnumerator alloc] initWithArray:arr] autorelease];
}

+ (id)enumeratorForCollection:(id <DPEnumeration>)collection {
	return [[[DPCollectionEnumerator alloc] initWithCollection:collection] autorelease];
}

- (id)nextObject {
	return nil;
}

- (NSArray *)allObjects {
	NSMutableArray *arr = [NSMutableArray arrayWithCapacity:[self count]];
	id obj;
	
	while ((obj = [self nextObject]))
		[arr addObject:obj];
	
	return obj;
}

- (BOOL)isValid {
	return NO;
}

- (unsigned)count {
	return 0U;
}

- (void)reset {
}

- (id)forward:(SEL)sel :(marg_list)args {
	id o;
	unsigned frameSize = dp_maxArgSizeForSelector(sel);
	
	while ((o = [self nextObject]))
		objc_msgSendv(o, sel, frameSize, args);
	
	return self;
}

BOOL DPIsEnumeratedArgument(void *ptr) {
	return (DPEnumerator *)ptr >= _DPEnumeratorsPool &&
			(DPEnumerator *)ptr <= _DPEnumeratorsPool + DP_MAX_ENUMERATORS;
}

@end

#define _DP_ENUM_OBJ 'z'

DP_STATIC_INLINE char *DPCopyArgumentTypesMap(const char *typedesc,
											  marg_list argsFrame,
											  Class enumeratorClass)
{
	unsigned i, argumentsCount = dp_getNumberOfArguments(typedesc);
	char *types = calloc(argumentsCount, sizeof(char));
	
	for (i = 0U; i < argumentsCount; i++) {
		const char *t;
		int offset;
		
		// Get the argument's type
		dp_getArgumentInfo(typedesc, i, &t, &offset);
		// Remember it
		types[i] = *t;
		// Check object values against our enumerator type
		// and mark the matches
		if (types[i] == _C_ID) {
			// Get the object from our arguments frame
			id obj = dp_margGetObject(argsFrame, offset, id);
			// If the object is an instance AND it's class is enumeratorClass or a subclass of it,
			// it's an enumerated argument.
			if (object_isInstance(obj))
				if (class_isSubclassOfClass(object_getClass(obj), enumeratorClass))
					types[i] = _DP_ENUM_OBJ;
		}
	}
	
	return types;
}

DP_STATIC_INLINE int *DPCopyOffsetsOfArguments(const char *typedesc) {
	unsigned i, argumentsCount = dp_getNumberOfArguments(typedesc);
	int *offsets = calloc(argumentsCount, sizeof(int));
	const char *t;
	
	for (i = 0U; i < argumentsCount; i++)
		dp_getArgumentInfo(typedesc, i, &t, &(offsets[i]));
	
	return offsets;
}

DP_STATIC_INLINE void DPUpdateEnumereratedArguments(marg_list origFrame,
													marg_list newFrame,
													const char *types,
													int *offsets,
													unsigned count,
													BOOL allowNil)
{
	unsigned i;
	for (i = 2U; i < count; i++) {
		if (types[i] == _DP_ENUM_OBJ) {
			// Get the enumerator from the original frame,
			// send it a -nextObject message, and that's our new value.
			id val = [dp_margGetObject(origFrame, offsets[i], id) nextObject];
			
			// If our value != nil than we simply apply it.
			// Otherwise, only if nil is a valid value (indicated by allowNil == YES)
			// we apply it. This means that if allowNil == NO and an enumerated
			// argument ends before time (i.e. it has less objects than the collection)
			// the last value returned from it will be used repeatedly.
			if (allowNil || val)
				// Set the new value
				dp_margSetObject(newFrame, offsets[i], id, val);
		}
	}
}

DP_STATIC_INLINE BOOL DPHasEnumeratedArguments(const char *types,
											   unsigned count)
{
	unsigned i;
	for (i = 2U; i < count; i++)
		if (types[i] == _DP_ENUM_OBJ)
			return YES;
	return NO;
}
							 

typedef unsigned (*DPUIMP)(id, SEL, ...);

// A generic, collection independent, implementation for -collect:
void DPCollectObjects(id <DPEnumeration> collection,
					  id resultsCollection,
					  DPMessage *message,
					  DPEnumerationAppendResultsCallBack callback)
{
	// A buffer to which our collection copies objects
	id buff[16];
	// Our enumeration state.
	// TODO: Consolidate it with 10.5's enumeration state?
	DPEnumerationState state = { 0, buff, NULL, NULL };
	// Cache the selector, frame length and the frame to save some useless messaging
	SEL sel = [message selector];
	unsigned frameLength = [message sizeOfArguments];
	marg_list origFrame = [message arguments];
	// A copy of the frame. It's what we use if there are enumerated arguments (DPEnumerator)
	// in the original frame. This is to avoid modifying the frame of our message.
	marg_list frame = origFrame;
	// IMP cacheing is good for large loops
	SEL enumerateWithStateSel = @selector(enumerateWithState:objects:count:);
	DPUIMP enumerateWithStateImp = (DPUIMP)[(NSObject *)collection methodForSelector:enumerateWithStateSel];
	// Get the first 16 objects from our collection
	// This is the same as [collection enumerateWithState:&state objects:buff count:16]
	unsigned count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	// An array of chars representing the types of our method's arguments
	char *types = NULL;
	// The offset of each argument in our marg_list
	int *offsets = NULL;
	// Total number of arguments which is also the size of the two arrays above
	unsigned argsCount = 0U;
	
	// Give up now if our collection is empty
	if (count == 0U)
		goto cleanup;
	
	// Get the type encoding we expect from the first object we have.
	// XXX What to do if the object can't respond to this method and returns NULL?
	// Sooner ot later this will cause us to crash and/or throw an exception.
	const char *typeEncoding = [state.items[0] typeEncodingForMessage:message];
	argsCount = dp_getNumberOfArguments(typeEncoding);
	
	if (argsCount > 2) {
		// Get a map of our arguments' types
		types = DPCopyArgumentTypesMap(typeEncoding, origFrame, [DPEnumerator class]);
		
		if (DPHasEnumeratedArguments(types, argsCount)) {
			unsigned int len = [message realFrameSize];
			
			// Copy our arguments frame
			frame = dp_marg_malloc(len);
			memcpy(frame, origFrame, len);
			
			// Get offsets as well
			offsets = DPCopyOffsetsOfArguments(typeEncoding);
		}
	}
	
	// Grab all objects from our collection, message them, and store the results
	while (count > 0U) {
		unsigned i;
		id results[16];
		unsigned resultsCount = 0;
		
		// Grab the results of all objects
		for (i = 0U; i < count; i++) {
			// Update any enumerated arguments if needed.
			// If any enumerated arguments contain less objects than our collection has
			// nil values will be used. This might be useful if the objects in the collection expect
			// nil, but otherwise it'll probably cause in assertion failure in the object's
			// implementation (which is not really our fault).
			// For example, if arr1 contains 5 strings and arr2 contains 3 strings,
			// [arr1 collect:MSG(stringByAppendingString:[arr2 each])] will throw an exception
			// when trying to append nil to strings 4 and 5.
			// NOTE: The last argument to DPUpdateEnumereratedArguments() indicates whether to use
			// nil when an enumerated argument is empty or to use the last object repeatedly.
			if (offsets != NULL)
				DPUpdateEnumereratedArguments(origFrame, frame, types, offsets, argsCount, YES);
			
			// Send the message and store its result
			results[resultsCount] = objc_msgSendv(state.items[i], sel, frameLength, frame);
			++resultsCount;
			
			// If our results buffer gets full, empty it and append the results
			if (resultsCount == 16) {
				// Append the results to the results collection
				callback(resultsCollection, &state, results, resultsCount);
				// Reset our counter
				resultsCount = 0;
			}
		}
		
		// Append the results to the results collection
		if (resultsCount)
			callback(resultsCollection, &state, results, resultsCount);
		
		// Get the next 16 objects from our collection
		count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	}
	
	// Clean up
	if (argsCount > 2U) {
		free(types);
		
		if (offsets) {
			free(offsets);
			free(frame);
		}
	}
cleanup:
	// Finally, invoke the release callback and return
	if (state.release != NULL)
		state.release(&state);
}

typedef BOOL (*DPBOOLMSGV)(id, SEL, unsigned arg_size, marg_list arg_frame);

// A generic, collection independent, implementation for -selectWhere:
// and -rejectWhere:
void DPSelectObjects(id <DPEnumeration> collection,
					 id resultsCollection,
					 DPMessage **messages,
					 unsigned messageCount,
					 BOOL returnValue,
					 DPEnumerationAppendResultsCallBack callback)
{
	// A buffer to which our collection copies objects
	id buff[16];
	// Our enumeration state.
	// TODO: Consolidate it with 10.5's enumeration state?
	DPEnumerationState state = { 0, buff, NULL, NULL };
	// Cache the selector, frame length and the frame to save some useless messaging
	SEL sel[messageCount];
	unsigned frameLength[messageCount];
	marg_list origFrame[messageCount];
	// A copy of the frame. It's what we use if there are enumerated arguments (DPEnumerator)
	// in the original frame. This is to avoid modifying the frame of our message.
	marg_list frame[messageCount];
	// IMP cacheing is good for large loops
	SEL enumerateWithStateSel = @selector(enumerateWithState:objects:count:);
	DPUIMP enumerateWithStateImp = (DPUIMP)[(NSObject *)collection methodForSelector:enumerateWithStateSel];
	// Get the first 16 objects from our collection
	// This is the same as [collection enumerateWithState:&state objects:buff count:16]
	unsigned count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	// An array of chars representing the types of our method's arguments
	char *types[messageCount];
	// The offset of each argument in our marg_list
	int *offsets[messageCount];
	// Total number of arguments which is also the size of the two arrays above
	unsigned argsCount[messageCount];
	
	// Just an ordinary index variable
	unsigned i;
	
	// Give up now if our collection is empty
	if (count == 0U)
		goto cleanup;
	
	BOOL initialized = NO;
	
	// Grab all objects from our collection, message them, and store the results
	while (count > 0U) {
		id results[16];
		unsigned resultsCount = 0;
		i = 0;
		
		// Initialize everything if needed
		if (__builtin_expect(!initialized, 0)) {
			id obj = state.items[0];
			
			// We don't have the privilege DPCollectObjects() has of working with a single
			// message, so we must initialize everything in a loop. Fun.
			// Everything here assumes the first object is a model to all others.
			for (i = 0; i < messageCount; i++) {
				// Cache all selectors, frame lengths and argument lists
				sel[i] = [messages[i] selector];
				frameLength[i] = [messages[i] sizeOfArguments];
				origFrame[i] = [messages[i] arguments];
				
				// Get the type encoding we expect from the first object we have.
				// XXX: What to do if the object can't respond to this method and returns NULL?
				// Sooner ot later this will cause us to crash and/or throw an exception.
				const char *typeEncoding = [obj typeEncodingForMessage:messages[i]];
				argsCount[i] = dp_getNumberOfArguments(typeEncoding);
				
				// NULL offsets means no enumerated arguments
				offsets[i] = NULL;
				// Our frame is the original one
				frame[i] = origFrame[i];
				
				if (argsCount[i] > 2) {
					// Get a map of our arguments' types
					types[i] = DPCopyArgumentTypesMap(typeEncoding, origFrame[i], [DPEnumerator class]);
					
					if (DPHasEnumeratedArguments(types[i], argsCount[i])) {
						unsigned int len = [messages[i] realFrameSize];
						
						// Copy our arguments frame
						frame[i] = dp_marg_malloc(len);
						memcpy(frame[i], origFrame[i], len);
						
						// Get offsets as well
						offsets[i] = DPCopyOffsetsOfArguments(typeEncoding);
					}
				}
				
				// Update any enumerated arguments
				if (offsets[i] != NULL)
					DPUpdateEnumereratedArguments(origFrame[i], frame[i], types[i], offsets[i], argsCount[i], YES);
				
				// The last message should return BOOL.
				// If it's YES, we add the original object to our results array.
				if (__builtin_expect(i == messageCount - 1, 0)) {
					if (((DPBOOLMSGV)(objc_msgSendv))(obj, sel[i],
													  frameLength[i],
													  frame[i]) == returnValue)
					{
						results[resultsCount] = state.items[0];
						++resultsCount;
					}
				} else {
					// Send the message to our object and use the result
					obj = objc_msgSendv(obj, sel[i], frameLength[i], frame[i]);
				}
			}
			
			// We're good to go
			initialized = YES;
			// Now start the loop from the second object
			i = 1;
		}
		
		// Grab the results of all objects
		for (/* i is initialized above */; i < count; i++) {
			unsigned j = 0;
			id obj = state.items[i];
			
			for (j = 0; j < messageCount - 1; j++) {
				// Update any enumerated arguments if needed.
				// If any enumerated arguments contain less objects than our collection has
				// nil values will be used. This might be useful if the objects in the collection expect
				// nil, but otherwise it'll probably cause in assertion failure in the object's
				// implementation (which is not really our fault).
				// For example, if arr1 contains 5 strings and arr2 contains 3 strings,
				// [arr1 collect:MSG(stringByAppendingString:[arr2 each])] will throw an exception
				// when trying to append nil to strings 4 and 5.
				// NOTE: The last argument to DPUpdateEnumereratedArguments() indicates whether to use
				// nil when an enumerated argument is empty or to use the last object repeatedly.
				if (offsets[j] != NULL)
					DPUpdateEnumereratedArguments(origFrame[j], frame[j], types[j],
												  offsets[j], argsCount[j], YES);
			
				// Send the message and store its result
				obj = objc_msgSendv(obj, sel[j], frameLength[j], frame[j]);
			}
			
			if (offsets[j] != NULL)
				DPUpdateEnumereratedArguments(origFrame[j], frame[j], types[j],
											  offsets[j], argsCount[j], YES);
			
			// The last message should return BOOL.
			// If it's YES, we add the original object to our results array.
			if (((DPBOOLMSGV)(objc_msgSendv))(obj, sel[j],
											  frameLength[j],
											  frame[j]) == returnValue)
			{
				results[resultsCount] = state.items[i];
				++resultsCount;
				
				if (resultsCount == 16) {
					// Append the results to the results collection
					callback(resultsCollection, &state, results, resultsCount);
					// Reset our counter
					resultsCount = 0;
				}
			}
		}
		
		// Append the results to the results collection
		if (resultsCount)
			callback(resultsCollection, &state, results, resultsCount);
		
		// Get the next 16 objects from our collection
		count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	}
	
	// Clean up
	for (i = 0; i < messageCount; i++) {
		if (argsCount[i] > 2U) {
			free(types[i]);
			
			if (offsets[i]) {
				free(offsets[i]);
				free(frame[i]);
			}
		}
	}
cleanup:
	// Finally, invoke the release callback and return
		if (state.release != NULL)
			state.release(&state);
}

id DPFindObject(id <DPEnumeration> collection,
				  DPMessage **messages,
				  unsigned messageCount,
				  BOOL returnValue)
{
	// A buffer to which our collection copies objects
	id buff[16];
	// Our enumeration state.
	// TODO: Consolidate it with 10.5's enumeration state?
	DPEnumerationState state = { 0, buff, NULL, NULL };
	// Cache the selector, frame length and the frame to save some useless messaging
	SEL sel[messageCount];
	unsigned frameLength[messageCount];
	marg_list origFrame[messageCount];
	// A copy of the frame. It's what we use if there are enumerated arguments (DPEnumerator)
	// in the original frame. This is to avoid modifying the frame of our message.
	marg_list frame[messageCount];
	// IMP cacheing is good for large loops
	SEL enumerateWithStateSel = @selector(enumerateWithState:objects:count:);
	DPUIMP enumerateWithStateImp = (DPUIMP)[(NSObject *)collection methodForSelector:enumerateWithStateSel];
	// Get the first 16 objects from our collection
	// This is the same as [collection enumerateWithState:&state objects:buff count:16]
	unsigned count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	// An array of chars representing the types of our method's arguments
	char *types[messageCount];
	// The offset of each argument in our marg_list
	int *offsets[messageCount];
	// Total number of arguments which is also the size of the two arrays above
	unsigned argsCount[messageCount];
	// Our return value
	id result = nil;
	
	// Just an ordinary index variable
	unsigned i;
	
	// Give up now if our collection is empty
	if (count == 0U)
		goto cleanup;
	
	BOOL initialized = NO;
	
	// Grab all objects from our collection, message them, and store the results
	while (count > 0U) {
		i = 0;
		
		// Initialize everything if needed
		if (__builtin_expect(!initialized, 0)) {
			id obj = state.items[0];
			
			// We don't have the privilege DPCollectObjects() has of working with a single
			// message, so we must initialize everything in a loop. Fun.
			// Everything here assumes the first object is a model to all others.
			for (i = 0; i < messageCount; i++) {
				// Cache all selectors, frame lengths and argument lists
				sel[i] = [messages[i] selector];
				frameLength[i] = [messages[i] sizeOfArguments];
				origFrame[i] = [messages[i] arguments];
				
				// Get the type encoding we expect from the first object we have.
				// XXX: What to do if the object can't respond to this method and returns NULL?
				// Sooner ot later this will cause us to crash and/or throw an exception.
				const char *typeEncoding = [obj typeEncodingForMessage:messages[i]];
				argsCount[i] = dp_getNumberOfArguments(typeEncoding);
				
				// NULL offsets means no enumerated arguments
				offsets[i] = NULL;
				// Our frame is the original one
				frame[i] = origFrame[i];
				
				if (argsCount[i] > 2) {
					// Get a map of our arguments' types
					types[i] = DPCopyArgumentTypesMap(typeEncoding, origFrame[i], [DPEnumerator class]);
					
					if (DPHasEnumeratedArguments(types[i], argsCount[i])) {
						unsigned int len = [messages[i] realFrameSize];
						
						// Copy our arguments frame
						frame[i] = dp_marg_malloc(len);
						memcpy(frame[i], origFrame[i], len);
						
						// Get offsets as well
						offsets[i] = DPCopyOffsetsOfArguments(typeEncoding);
					}
				}
				
				// Update any enumerated arguments
				if (offsets[i] != NULL)
					DPUpdateEnumereratedArguments(origFrame[i], frame[i], types[i], offsets[i], argsCount[i], YES);
				
				// Send the message to our object and use the result
				obj = objc_msgSendv(obj, sel[i], frameLength[i], frame[i]);
				
				// The last message should return BOOL.
				// The first to return 'returnValue' is our match.
				if (i == messageCount - 1 && (BOOL)(long)obj == returnValue) {
					result = state.items[0];
					// Go do some final cleanups and return
					goto finish;
				}
			}
			
			// We're good to go
			initialized = YES;
			// Now start the loop from the second object
			i = 1;
		}
		
		// Grab the results of all objects
		for (/* i is initialized above */; i < count; i++) {
			unsigned j = 0;
			id obj = state.items[i];
			
			for (j = 0; j < messageCount - 1; j++) {
				// Update any enumerated arguments if needed.
				// If any enumerated arguments contain less objects than our collection has
				// nil values will be used. This might be useful if the objects in the collection expect
				// nil, but otherwise it'll probably cause in assertion failure in the object's
				// implementation (which is not really our fault).
				// For example, if arr1 contains 5 strings and arr2 contains 3 strings,
				// [arr1 collect:MSG(stringByAppendingString:[arr2 each])] will throw an exception
				// when trying to append nil to strings 4 and 5.
				// NOTE: The last argument to DPUpdateEnumereratedArguments() indicates whether to use
				// nil when an enumerated argument is empty or to use the last object repeatedly.
				if (offsets[j] != NULL)
					DPUpdateEnumereratedArguments(origFrame[j], frame[j], types[j],
												  offsets[j], argsCount[j], YES);
				
				// Send the message and store its result
				obj = objc_msgSendv(obj, sel[j], frameLength[j], frame[j]);
			}
			
			if (offsets[j] != NULL)
				DPUpdateEnumereratedArguments(origFrame[j], frame[j], types[j],
											  offsets[j], argsCount[j], YES);
			
			// The last message should return BOOL.
			// The first object to return 'returnValue' is our match.
			if (((DPBOOLMSGV)(objc_msgSendv))(obj, sel[j],
											  frameLength[j],
											  frame[j]) == returnValue)
			{
				result = state.items[i];
				// Go do some final cleanups and return
				goto finish;
			}
		}
		
		// Get the next 16 objects from our collection
		count = enumerateWithStateImp(collection, enumerateWithStateSel, &state, buff, 16);
	}
	
finish:
	// Clean up
	for (i = 0; i < messageCount; i++) {
		if (argsCount[i] > 2U) {
			free(types[i]);
			
			if (offsets[i]) {
				free(offsets[i]);
				free(frame[i]);
			}
		}
	}
cleanup:
	// Finally, invoke the release callback and return
		if (state.release != NULL)
			state.release(&state);
	
	return result;
}


@implementation NSArray (DPCollectionEnumeration)

static void DPNSArrayAppendResults (id resultsCollection,
									DPEnumerationState *state,
									id *objects,
									unsigned count)
{
	unsigned i;
	for (i = 0U; i < count; i++)
		CFArrayAppendValue((CFMutableArrayRef)resultsCollection, objects[i]);
}

- (unsigned)enumerateWithState:(DPEnumerationState *)state
					   objects:(id *)buff
						 count:(unsigned)buffLen
{
	CFIndex count = CFArrayGetCount((CFArrayRef)self);
	
	if (state->state < count) {
		CFIndex len = (state->state + buffLen < count) ? buffLen : (count - state->state);
		
		CFArrayGetValues((CFArrayRef)self, CFRangeMake(state->state, len), (const void **)buff);
		state->state += len;
		return len;
	}
	
	return 0;
}


- (id)each {
	//return [DPEnumerator enumeratorForArray:self];
	return [DPEnumerator enumeratorForCollection:self];
}
	

- (id)collect:(DPMessage *)argumentMessage {
	NSMutableArray *arr = [NSMutableArray arrayWithCapacity:[self count]];
	
	DPCollectObjects(self, arr, argumentMessage, DPNSArrayAppendResults);
	return arr;
}

- (id)pick:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count] / 2];
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	DPSelectObjects(self, result, messages, count, val, DPNSArrayAppendResults);
	return result;
}

- (id)selectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)rejectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:NO where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)find:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	return DPFindObject(self, messages, count, val);
}

- (id)findObjectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self find:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

@end


@implementation NSSet (DPCollectionEnumeration)

static void DPNSSetAppendResults (id resultsCollection,
								  DPEnumerationState *state,
								  id *objects,
								  unsigned count)
{
	unsigned i;
	for (i = 0U; i < count; i++)
		CFSetAddValue((CFMutableSetRef)resultsCollection, objects[i]);
}

static void DPNSSetEnumerationCleanup(DPEnumerationState *state) {
	if (state->items)
		free(state->items);
}

- (unsigned)enumerateWithState:(DPEnumerationState *)state
					   objects:(id *)buff
						 count:(unsigned)buffLen
{
	CFIndex count = CFSetGetCount((CFSetRef)self);
	
	// Just copy all objects at once
	if (state->state == 0 && count > 0) {
		id *objects = calloc(count, sizeof(id));
		
		CFSetGetValues((CFSetRef)self, (const void **)objects);
		state->items = objects;
		state->state = count - 1;
		state->release = (void *)DPNSSetEnumerationCleanup;
		
		return count;
	}
	
	return 0;
}


- (id)each {
	CFIndex count = CFSetGetCount((CFSetRef)self);
	id *objects = calloc(count, sizeof(id));
	
	CFSetGetValues((CFSetRef)self, (const void **)objects);
	return [DPEnumerator enumeratorWithObjects:objects
										 count:count
								  freeWhenDone:YES];
}


- (id)collect:(DPMessage *)argumentMessage {
	NSMutableSet *arr = [NSMutableSet setWithCapacity:[self count]];
	
	DPCollectObjects(self, arr, argumentMessage, DPNSSetAppendResults);
	return arr;
}

- (id)pick:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	NSMutableSet *result = [NSMutableSet setWithCapacity:[self count] / 2];
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	DPSelectObjects(self, result, messages, count, val, DPNSSetAppendResults);
	return result;
}

- (id)selectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)rejectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:NO where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)find:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	return DPFindObject(self, messages, count, val);
}

- (id)findObjectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self find:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

@end

@implementation NSDictionary (DPCollectionEnumeration)

static void DPNSDictionaryAppendResults (id resultsCollection,
										 DPEnumerationState *state,
										 id *objects,
										 unsigned count)
{
	unsigned i;
	id *obj = state->items;
	// We hold the keys in the info field
	id *key = state->info;
	
	for (i = 0U; i < count; i++) {
		int j;
		
		// Find the index of the key matching the object we need to apply
		for (j = i; (*obj) != objects[i]; ++j)
			++obj;
		
		CFDictionaryAddValue((CFMutableDictionaryRef)resultsCollection, key[j], *obj);
	}
}

static void DPNSDictionaryEnumerationCleanup(DPEnumerationState *state) {
	if (state->items)
		free(state->items);
	
	if (state->info)
		free(state->info);
}

- (unsigned)enumerateWithState:(DPEnumerationState *)state
					   objects:(id *)buff
						 count:(unsigned)buffLen
{
	CFIndex count = CFDictionaryGetCount((CFDictionaryRef)self);
	
	// Just copy all objects at once
	if (state->state == 0 && count > 0) {
		id *values = calloc(count, sizeof(id));
		id *keys = calloc(count, sizeof(id));
		
		CFDictionaryGetKeysAndValues((CFDictionaryRef)self,
									 (const void **)keys,
									 (const void **)values);
		state->items = values;
		state->info = keys;
		state->state = count - 1;
		state->release = (void *)DPNSDictionaryEnumerationCleanup;
		
		return count;
	}
	
	return 0;
}


- (id)each {
	CFIndex count = CFSetGetCount((CFSetRef)self);
	id *objects = calloc(count, sizeof(id));
	
	CFDictionaryGetKeysAndValues((CFDictionaryRef)self,
								 NULL, (const void **)objects);
	return [DPEnumerator enumeratorWithObjects:objects
										 count:count
								  freeWhenDone:YES];
}


- (id)collect:(DPMessage *)argumentMessage {
	NSMutableDictionary *arr = [NSMutableDictionary dictionaryWithCapacity:[self count]];
	
	DPCollectObjects(self, arr, argumentMessage, DPNSDictionaryAppendResults);
	return arr;
}

- (id)pick:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:[self count] / 2];
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	DPSelectObjects(self, result, messages, count, val, DPNSDictionaryAppendResults);
	return result;
}

- (id)selectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)rejectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self pick:NO where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

- (id)find:(BOOL)val where:(DPMessage *)firstMsg additionalMessages:(va_list)msgs {
	va_list va;
	unsigned i, count = 1;
	
	va_copy(va, msgs);
	while (va_arg(va, id))
		++count;
	va_end(va);
	
	DPMessage *messages[count];
	messages[0] = firstMsg;
	
	va_copy(va, msgs);
	
	for (i = 1; i < count; i++) {
		messages[i] = va_arg(va, DPMessage *);
	}
	
	va_end(va);
	
	return DPFindObject(self, messages, count, val);
}

- (id)findObjectWhere:(DPMessage *)firstMessage, ... {
	va_list va;
	id r = nil;
	
	NSAssert(firstMessage != nil, @"Passed nil message");
	
	va_start(va, firstMessage);
	r = [self find:YES where:firstMessage additionalMessages:va];
	va_end(va);
	
	return r;
}

@end


@interface NSObject (runtimeMethods)
- (id)forward:(SEL)sel :(marg_list)args;
@end


@implementation NSObject (DPHOMGoodies)

- (id)collect:(DPMessage *)msg {
	SEL sel = [msg selector];
	marg_list origFrame = [msg arguments], frame = origFrame;
	unsigned int frameSize = [msg sizeOfArguments];
	const char *typedesc = [self typeEncodingForMessage:msg];
	
	if (!typedesc)
		return nil;
	
	unsigned argsCount = dp_getNumberOfArguments(typedesc);
	char *types = DPCopyArgumentTypesMap(typedesc, frame, [DPEnumerator class]);
	int *offsets = DPCopyOffsetsOfArguments(typedesc);
	unsigned numberOfIterations = 0, i;
	id result = nil;
	
	for (i = 2; i < argsCount; i++) {
		if (types[i] == _DP_ENUM_OBJ) {
			unsigned c = [dp_margGetObject(frame, offsets[i], DPEnumerator *) count];
			numberOfIterations = MAX(numberOfIterations, c);
		}
	}
	
	if (numberOfIterations == 0) {
		result = dp_msgSendv(self, sel, frame);
	} else {
		char rt;
		
		dp_getReturnType(typedesc, &rt, 1);
		
		// Copy the frame
		size_t fsize = [msg realFrameSize];
		frame = dp_marg_malloc(fsize);
		memcpy(frame, origFrame, fsize);
		
		// Allocate an array for the results
		if (rt == _C_ID || rt == _C_CLASS)
			result = [NSMutableArray arrayWithCapacity:numberOfIterations];
		
		// Let's do it
		for (i = 0; i < numberOfIterations; ++i) {
			DPUpdateEnumereratedArguments(origFrame, frame, types, offsets, argsCount, YES);
			
			if (rt == _C_ID || rt == _C_CLASS)
				[result addObject:objc_msgSendv(self, sel, frameSize, frame)];
			else
				objc_msgSendv(self, sel, frameSize, frame);
		}
		
		free(frame);
	}
	
	// Clean up
	free(offsets);
	free(types);
	
	return result;
}

- (id)receive:(DPMessage *)msg {
	SEL sel = [msg selector];
	marg_list origFrame = [msg arguments], frame = origFrame;
	unsigned int frameSize = [msg sizeOfArguments];
	const char *typedesc = [self typeEncodingForMessage:msg];
	
	if (!typedesc)
		return nil;
	
	unsigned argsCount = dp_getNumberOfArguments(typedesc);
	char *types = DPCopyArgumentTypesMap(typedesc, frame, [DPEnumerator class]);
	int *offsets = DPCopyOffsetsOfArguments(typedesc);
	unsigned numberOfIterations = 0, i;
	id result = nil;
	
	for (i = 2; i < argsCount; i++) {
		if (types[i] == _DP_ENUM_OBJ) {
			unsigned c = [dp_margGetObject(frame, offsets[i], DPEnumerator *) count];
			numberOfIterations = MAX(numberOfIterations, c);
		}
	}
	
	if (numberOfIterations == 0) {
		result = dp_msgSendv(self, sel, frame);
	} else {
		char rt;
		
		dp_getReturnType(typedesc, &rt, 1);
		
		// Copy the frame
		size_t fsize = [msg realFrameSize];
		frame = dp_marg_malloc(fsize);
		memcpy(frame, origFrame, fsize);
		
		// Let's do it
		for (i = 0; i < numberOfIterations - 1; ++i) {
			DPUpdateEnumereratedArguments(origFrame, frame, types, offsets, argsCount, YES);
			objc_msgSendv(self, sel, frameSize, frame);
		}
		
		DPUpdateEnumereratedArguments(origFrame, frame, types, offsets, argsCount, YES);
		result = objc_msgSendv(self, sel, frameSize, frame);
		
		free(frame);
	}
	
	// Clean up
	free(offsets);
	free(types);
	
	return result;
}

- (id)ifResponds:(DPMessage *)msg {
	if ([self respondsToSelector:[msg selector]])
		return [msg sendTo:self];
	return nil;
}

@end

@implementation NSString (DPHOMCompatibility)

/*
 * Given a format string and an initial offset, this function builds a parital types description
 * from the given format string. For example, given offset of 8 and a format string "%@",
 * the result will be "@8" and the frame size (if not NULL) will be 12 (8 + sizeof(id)).
 * If startOffset is 0, the function assumes a method accepting a format string followed
 * by variable arguments, in which case it'll also take in account the self and _cmd parameters
 * of the method. Otherwise it's up to you to append them at the beginning of the description.
 * In any case, you must add the return type of the method at the begnning of the description
 * before passing it to anyone (otherwise it won't be a valid decscription).
 */
NSMutableString *DPTypeDescriptionForFormatString(NSString *str, unsigned startOffset, unsigned *frameSize) {
	// It's probably OK to hardcode the values of @encode() but let's do it the right way
	NSMutableString *description = startOffset == 0 ? [NSMutableString stringWithFormat:@"%s0"  // self (offset 0,
																								// assuming no struct return)
																						"%s%u"  // _cmd
																						"%s%u", // format string
																		@encode(id),  // @encode(self)
																		@encode(SEL), // encode(_cmd)
																		sizeof(id),	  // _cmd comes right after self
																		@encode(id),  // format string
																		sizeof(id) + sizeof(SEL)]
													: [NSMutableString string];
	// I assume all constant string these days are constant CFStrings rather than NSConstantString,
	// so this *should* be a bit faster than repeatedly calling characterAtIndex: but I haven't
	// measured it. Ya ya, premature optimization, evil, whatever.
	CFStringInlineBuffer buff;
	unsigned i = 1, len = [str length];
	unsigned offset = startOffset == 0 ? sizeof(id) * 2 + sizeof(SEL)	// self + _cmd + format
									   : startOffset;
	UniChar ch1, ch2;
	
	CFStringInitInlineBuffer((CFStringRef)str, &buff, CFRangeMake(0, len));
	ch1 = CFStringGetCharacterFromInlineBuffer(&buff, 0);
	
	while ((ch2 = CFStringGetCharacterFromInlineBuffer(&buff, i))) {
		if (ch1 == '%') {
			switch (ch2) {
				case '@':
					// Note that %@ can also mean a class (which has a different type encodign),
					// but id should be fine for everyone.
					[description appendFormat:@"%s%u", @encode(id), offset];
					offset += sizeof(id);
					break;
					
				case 'd':
				case 'D':
				case 'i':
					// Even though the docs say %d is for "32 bit integer (long)"
					// we must explicitly use int32_t because OSX64 is LP64 so long
					// will actually be 64 bit and not 32.
					[description appendFormat:@"%s%u", @encode(int32_t), offset];
					offset += sizeof(int32_t);
					break;
					
				case 'u':
				case 'U':
				case 'x':
				case 'X':
				case 'o':
				case 'O':
					[description appendFormat:@"%s%u", @encode(uint32_t), offset];
					offset += sizeof(uint32_t);
					break;
					
				case 'h': {
					// Read the next character
					++i;
					ch2 = CFStringGetCharacterFromInlineBuffer(&buff, i);
					switch (ch2) {
						case 'i':
							[description appendFormat:@"%s%u", @encode(int16_t), offset];
							offset += sizeof(int16_t);
							break;
							
						case 'u':
						case 'x':
						case 'X':
							[description appendFormat:@"%s%u", @encode(uint16_t), offset];
							offset += sizeof(uint16_t);
							break;
							
						default:
							break;
					}
					break;
				}
					
				case 'q': {
					// Read the next character
					++i;
					ch2 = CFStringGetCharacterFromInlineBuffer(&buff, i);
					switch (ch2) {
						case 'i':
							[description appendFormat:@"%s%u", @encode(int64_t), offset];
							offset += sizeof(int64_t);
							break;
							
						case 'u':
							[description appendFormat:@"%s%u", @encode(uint64_t), offset];
							offset += sizeof(uint64_t);
							break;
							
						default:
							break;
					}
					break;
				}
				
				// Warning: if someone passes a float instead of double
				// this may cause an overflow somewhere (depends on how
				// you get our arguments from). How does NSString handle
				// this?
				case 'f':
				case 'e':
				case 'E':
				case 'g':
				case 'G':
					[description appendFormat:@"%s%u", @encode(double), offset];
					offset += sizeof(double);
					break;
					
				case 'c':
					[description appendFormat:@"%s%u", @encode(unsigned char), offset];
					offset += sizeof(unsigned char);
					break;
					
				case 'C':
					[description appendFormat:@"%s%u", @encode(unichar), offset];
					offset += sizeof(unichar);
					break;
					
				case 's':
					[description appendFormat:@"%s%u", @encode(char *), offset];
					offset += sizeof(char *);
					break;
					
				case 'S':
					[description appendFormat:@"%s%u", @encode(unichar *), offset];
					offset += sizeof(unichar *);
					break;
					
				case 'p':
					[description appendFormat:@"%s%u", @encode(void *), offset];
					offset += sizeof(void *);
					break;
					
				default:
					// No need to reuse this character as it can't be a
					// valid format specifier.
					++i;
					ch1 = ch2;
					// goto is *NOT* always evil!!
					goto NoFormat;
			}
			
			// If we found a foramt specifier there's no need to repeat its
			// last character. Even worse, it may cause wrong behaviour.
			ch1 = CFStringGetCharacterFromInlineBuffer(&buff, ++i);
			++i;
		} else {
NoFormat:
			++i;
			ch1 = ch2;
		}
	}
	
	if (frameSize)
		*frameSize = offset + startOffset;
	
	if (startOffset == 0)
		// Apply the total length of the arguments frame as required by the runtime.
		[description insertString:[NSString stringWithFormat:@"%u", offset]
						  atIndex:0];
	
	return description;
}

// Support for initWithFormat:, stringByAppendingFormat:, stringWithFormat:,
// appendFormat: and initWithFormat:locale:. Fun.
- (const char *)typeEncodingForMessage:(DPMessage *)msg {
	SEL sel = [msg selector];
	
	if (sel == @selector(initWithFormat:) ||
		sel == @selector(stringByAppendingFormat:)||
		sel == @selector(stringWithFormat:))
	{
		NSMutableString *desc = DPTypeDescriptionForFormatString(dp_margGetObject([msg arguments], sizeof(id) + sizeof(SEL),
																				  NSString *), 0, NULL);
		// Set the return type
		[desc insertString:[NSString stringWithCString:@encode(id)]
				   atIndex:0];
		// We return a C string that'll automatically get freed in the
		// next autorelease cycle, together with our mutable string.
		return [desc cStringUsingEncoding:NSASCIIStringEncoding];
	} else if (sel == @selector(appendFormat:)) {
		NSMutableString *desc = DPTypeDescriptionForFormatString(dp_margGetObject([msg arguments], sizeof(id) + sizeof(SEL),
																				  NSString *), 0, NULL);
		// Set the return type
		[desc insertString:[NSString stringWithCString:@encode(void)]
				   atIndex:0];
		// We return a C string that'll automatically get freed in the
		// next autorelease cycle, together with our mutable string.
		return [desc cStringUsingEncoding:NSASCIIStringEncoding];
	} else if (sel == @selector(initWithFormat:locale:)) {
		unsigned frameSize;
		NSMutableString *desc = DPTypeDescriptionForFormatString(dp_margGetObject([msg arguments], sizeof(id) + sizeof(SEL),
																				  NSString *),
																 sizeof(id) * 2 + sizeof(SEL),
																 &frameSize);
		// Build the begining of our description which includes (by order):
		// the return type, total frame size, self + offset, _cmd + offset,
		// format string + offset, locale variable + offset
		[desc insertString:[NSString stringWithFormat:@"%s%u%s0%s%d", @encode(id), frameSize,
			@encode(id), @encode(SEL), sizeof(id) + sizeof(SEL)]
				   atIndex:0];
		// We return a C string that'll automatically get freed in the
		// next autorelease cycle, together with our mutable string.
		return [desc cStringUsingEncoding:NSASCIIStringEncoding];
	}
	
	return [super typeEncodingForMessage:msg];
}

@end


@implementation NSObject (DPIntrospectionAdditions)

- (const char *)typeEncodingForSelector:(SEL)sel {
	Method m = dp_getMethod(self, sel);
	return m ? method_getTypeEncoding(m) : NULL;
}

- (const char *)typeEncodingForMessage:(DPMessage *)msg {
	return [self typeEncodingForSelector:[msg selector]];
}


/*
 * OK folks, here is a really cool voodoo.
 * As you probably know, the runtime's method lookup for a given class
 * scans its methods list and all its super classes, and if it can't find
 * a method it tries the instance methods list of the class's base class
 * (ya ya, I know it's weird).
 * The following code hacks around this behaviour and gets a nicer effect:
 * after the class and its super classes couldn't provide a method for
 * the selector, it looks up on the instance methods list of our class
 * and any of its super class, so the base class's implementation is invoked
 * only if non of the direct super classes could provide one.
 * This allows us to override only the instance methods in our custom NSObject
 * subclass, and still allow proper result when messaging our class.
 * See above how it's done with NSString.
 */
+ (const char *)typeEncodingForSelector:(SEL)sel {
	return (void *)(method_getImplementation(class_getInstanceMethod(self, _cmd))(self, _cmd, sel));
}

+ (const char *)typeEncodingForMessage:(DPMessage *)msg {
	return (void *)(method_getImplementation(class_getInstanceMethod(self, _cmd))(self, _cmd, msg));
}

@end
