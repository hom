/*
 * Buddy system memory allocator.
 *
 * The original buddy system memory allocation algorithm was taken from
 * "The Art of Computer Programming, Volume 1, 3rd Edition", by
 * Donald E. Knuth, pages 442-444.
 *
 * Written by Miguel Masmano Tello <mmasmano@disca.upv.es>
 * (C) 2002 Release under the terms of the GPL Version 2
 *
 */

#ifndef _BINBUDDY_H_
#define _BINBUDDY_H_


#include <string.h>
#include <stdlib.h>
#define PRINT_MSG(message) printf(message)
#define PRINT_MSG_1(message, arg) printf(message, arg)
#define PRINT_MSG_2(message, arg_1, arg_2) printf(message, arg_1, arg_2)
#define PRINT_MSG_6(message, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6) printf(message, arg_1, arg_2, arg_3, arg_4, arg_5, arg_6)

#define PRINT_DBG_C(message) printf(message)
#define PRINT_DBG_D(message) printf("%i", message);
//#define PRINT_DBG_F(message) printf("%f", message);
#define PRINT_DBG_H(message) printf("%x", message);

/* 
 * This function initialite all buffers needed  
 * by the buddy algorithm 
 */

int init_memory_pool (int max_chunk_log2_size,
		      int min_chunk_log2_size, char *initial_buffer);

void destroy_memory_pool(void);

/* These functions return log2 max and log2 min chunk size */
int max_chunk_log2_size(void);
int min_chunk_log2_size(void);

/* These functions return max and min chunk size */
int max_chunk_size(void); 
int min_chunk_size(void);

/* see man malloc */
void *rt_malloc (size_t size);
/* see man realloc */

void *rt_realloc(void *p, size_t new_len);

/* see man calloc */
void *rt_calloc(size_t nelem, size_t elem_size);

/*  
 * see man free 
 *
 * rt_free() is only guaranteed to work if ptr is the address of a block
 * allocated by rt_malloc() (and not yet freed). 
 */
void rt_free (void *ptr);

#ifdef __DEBUG__

/* memory_dump does a dumped of the memory context */
void memory_dump (void);

/* 
 * free_blocks_context does a dump
 * of free_chunks_array and its context 
 */

void free_blocks_context(void);

/* 
 * This function gives information about 
 * algorithm structures
 */

void statistics_data (void);

/* 
 * This function checks memory searching 
 * errors and incoherences 
 * return :
 * 0 if there aren't errors 
 * or 
 * -1 in other case
 */

int check_mem (void);

void global_state (int *free, int *used, int *overhead);

#endif // #ifdef __DEBUG__

#endif // #ifdef _BINBUDDY_H_
