/*#io
docCopyright("Steve Dekorte", 2002)
docLicense("BSD revised")
*/

#ifdef BYTEARRAY_C 
#define IO_IN_C_FILE
#endif
#include "Common_inline.h"
#ifdef IO_DECLARE_INLINES

#include <stdlib.h>
#include <stddef.h>

IOINLINE int IsPathSeparator(const char ch)
{
#ifdef ON_WINDOWS
  return (ch == '/') || (ch == '\\');
#else
  return (ch == '/');
#endif
}

IOINLINE size_t ByteArray_size(ByteArray *self) 
{ 
	return self->size; 
}

IOINLINE unsigned char *ByteArray_bytes(ByteArray *self) 
{ 
	return self->bytes; 
}

IOINLINE int ByteArray_compareData_size_(ByteArray *self, 
								 const unsigned char *b2, 
								 unsigned int l2)
{
	unsigned int l1 = self->size;
	unsigned int min = l1 < l2 ? l1 : l2;
	int cmp = memcmp(self->bytes, b2, min);
	
	if (cmp == 0)
	{
		if (l1 == l2) return 0;
		if (l1 < l2) return -1;
		return 1;
	}
	
	return cmp;
}

IOINLINE int ByteArray_compare_(ByteArray *self, ByteArray *other)
{
	return ByteArray_compareData_size_(self, other->bytes, other->size);
}

IOINLINE char *ByteArray_asCString(ByteArray *self) 
{ 
	return (char *)self->bytes; 
}

IOINLINE unsigned char ByteArray_at_(ByteArray *self, int pos) 
{ 
	return self->bytes[ByteArray_wrapPos_(self, pos)]; 
}

// type access ----------------------- 

IOINLINE float *ByteArray_floatPointerAt_(ByteArray *self, size_t i)
{
	size_t pos = (int)i * sizeof(float);
	if ((pos + sizeof(float)) > self->size) return NULL;
	return (float *)(self->bytes + pos);
}

// WARNING: no bounds check 

// 32 bit

IOINLINE size_t ByteArray_size32(ByteArray *self)
{ 
	return self->size / 4; 
}

 // hashing

#define BYTEARRAY_BYTEWISE_HASH 1

#ifdef BYTEARRAY_BYTEWISE_HASH

IOINLINE uintptr_t ByteArray_calcHash(ByteArray *self)
{
	size_t size = self->size;
	const unsigned char *bytes = self->bytes;
	uintptr_t h = 5381;
	
	while (size-- > 0)
	{
		h += (h << 5); // h(i) = (h(i-1) * 33) ^ key(i) 
		h ^= *bytes ++;
	}
	
	return h;
}

#else

IOINLINE uintptr_t ByteArray_calcHash(ByteArray *self)
{
	size_t size = self->size;
	const intptr_t *chunks = (uintptr_t *)self->bytes;
	
	ldiv_t d = ldiv((long)size, (long)sizeof(uintptr_t));
	long q = d.quot;
	long r = d.rem;
	uintptr_t h = 5381;
	
	while (q --)
	{
		h ^= *chunks;
		chunks ++;
	}
	
	if (r)
	{
		intptr_t rm = 0;
		memcpy(&rm, self->bytes - r, r);
		h ^= rm;
	}
	
	return h;
}

#endif

IOINLINE uintptr_t ByteArray_hash(ByteArray *self)
{
	if (!self->hash) 
	{
		self->hash = ByteArray_calcHash(self);
		if(self->hash == 0x0) self->hash = 0x1;
	}
	
	return self->hash;
}

IOINLINE int ByteArray_equalsWithHashCheck_(ByteArray *self, ByteArray *other)		
{
	if (self == other)
	{
		return 1;
	}
	else
	{
		uintptr_t h1 = ByteArray_hash(self);
		uintptr_t h2 = ByteArray_hash(other);
		
		if (h1 != h2) 
		{
			return 0;
		}
	}
	
	return ByteArray_equals_(self, other);
}

#undef IO_IN_C_FILE
#endif
