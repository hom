/*#io
docCopyright("Steve Dekorte", 2002)
docCopyright("Marc Fauconneau", 2007)
docLicense("BSD revised")
docDescription("""
    PHash - Cuckoo Hash
    keys and values are references (they are not copied or freed)
    key pointers are assumed unique
""")
*/

#ifndef CHASH_DEFINED
#define CHASH_DEFINED 1

#include "Common.h"
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct 
{
    void *key;
    void *value;
} PHashRecord;

typedef struct 
{
    PHashRecord *records;		// contains the two tables
    
	unsigned int log2tableSize;	// log2(tableSize)
    unsigned int tableSize;		// total number of records per table
    unsigned int numKeys;		// number of used records
	unsigned int index;			// enumeration index

    unsigned int mask;		    // hash bit mask
	PHashRecord nullRecord;		// for lookup misses
    unsigned int balance;		// to balance tables
    unsigned int maxLoops;		// cuckoo effort limit
    unsigned int maxKeys;		// density limit
} PHash;

BASEKIT_API void PHash_print(PHash *self); // to debug

BASEKIT_API PHash *PHash_new(void);
BASEKIT_API void PHash_free(PHash *self);
BASEKIT_API PHash *PHash_clone(PHash *self);
BASEKIT_API void PHash_copy_(PHash *self, PHash *other);

BASEKIT_API PHashRecord* PHash_cuckoo_(PHash *self, PHashRecord* record);
BASEKIT_API void PHash_grow(PHash *self);
BASEKIT_API void PHash_growWithRecord(PHash *self, PHashRecord* record);

BASEKIT_API size_t PHash_memorySize(PHash *self);
BASEKIT_API void PHash_compact(PHash *self);

BASEKIT_API void *PHash_firstValue(PHash *self);
BASEKIT_API void *PHash_nextValue(PHash *self);

BASEKIT_API void *PHash_firstKey(PHash *self);
BASEKIT_API void *PHash_nextKey(PHash *self);

BASEKIT_API unsigned int PHash_count(PHash *self);
BASEKIT_API unsigned int PHash_doCount(PHash *self);
BASEKIT_API unsigned int PHash_countRecords_size_(unsigned char *records, unsigned int size);

BASEKIT_API PHashRecord *PHash_recordAtIndex_(PHash *self, unsigned int index);
BASEKIT_API void *PHash_keyAt_(PHash *self, unsigned int index);
BASEKIT_API void *PHash_valueAt_(PHash *self, unsigned int index);
BASEKIT_API int PHash_indexForValue_(PHash *self, void *v);
BASEKIT_API void *PHash_firstKeyForValue_(PHash *self, void *v);

// --- perform -------------------------------------------------- 

typedef BASEKIT_API void (PHashDoCallback)(void *);
BASEKIT_API void PHash_do_(PHash *self, PHashDoCallback *callback);

typedef int (PHashDetectCallback)(void *);
BASEKIT_API void *PHash_detect_(PHash *self, PHashDetectCallback *callback); // returns matching key
BASEKIT_API void PHash_doOnKeys_(PHash *self, PHashDoCallback *callback);
BASEKIT_API void PHash_removeValue_(PHash *self, void *value);

#include "PHash_inline.h"

#ifdef __cplusplus
}
#endif
#endif

