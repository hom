/*#io
docCopyright("Steve Dekorte", 2002)
docCopyright("Marc Fauconneau", 2007)
docLicense("BSD revised")
*/

#define CHASH_C
#include "PHash.h"
#undef CHASH_C
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void PHash_print(PHash* self)
{
	printf("self->log2tableSize = %d\n", self->log2tableSize);
    printf("self->tableSize = %d\n", self->tableSize);
    printf("self->numKeys = %d\n", self->numKeys);

    printf("self->mask = %d\n", self->mask);
    printf("self->balance = %d\n", self->balance);
    printf("self->maxLoops = %d\n", self->maxLoops);
    printf("self->maxKeys = %d\n", self->maxKeys);

	printf("self->nullRecord.key = %d\n", self->nullRecord.key);
	printf("self->nullRecord.value = %d\n", self->nullRecord.value);

	printf("\nmemory usage : %d bytes\n", PHash_memorySize(self));
	printf("\ndensity : %f \n", (self->numKeys*sizeof(PHashRecord))/(double)PHash_memorySize(self));

	{
		unsigned int i,j;
		int count[2] = {0,0};
		
		for (j = 0; j < 2; j ++)
		{
			for (i = 0; i < self->tableSize; i ++)
			{
				PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
				if (NULL == record->key)
				{
					if (NULL == record->value)
						printf("_");
					else
						printf("!");
				}
				else 
				{
					printf("x");
					count[j]++;
				}
			}
			printf("\n");
		}
		printf("balance : %d / %d [%1.3f]\n", count[0], count[1], (count[0]-count[1])/(double)(count[0]+count[1]) );
	}
}

void PHash_tableInit_(PHash* self, int log2tableSize) 
{
	if (log2tableSize > 20)
		printf("ouuups");
	self->log2tableSize = log2tableSize;
	self->tableSize = 1<<self->log2tableSize;
	self->records = (PHashRecord *)calloc(1, sizeof(PHashRecord) * self->tableSize * 2);
	self->mask = self->tableSize-1;
	self->maxLoops = self->tableSize;
	self->maxKeys = self->tableSize;
}

PHash *PHash_new(void) 
{
	PHash *self = (PHash *)calloc(1, sizeof(PHash)); 
	self->numKeys = 0;
	PHash_tableInit_(self, 1);
	//printf("ok");
	return self; 
}

PHash *PHash_clone(PHash *self)
{
	PHash *child = PHash_new();
	PHash_copy_(child, self);
	return child;
}

void PHash_free(PHash *self)
{
	free(self->records);
	free(self);
}

size_t PHash_memorySize(PHash *self)
{ 
	return sizeof(PHash) + (self->tableSize * 2 * sizeof(PHashRecord));
}

void PHash_compact(PHash *self)
{ 
	printf("need to implement PHash_compact\n"); 
}

void PHash_copy_(PHash *self, PHash *other)
{
	PHashRecord *records = self->records;
	memcpy(self, other, sizeof(PHash));
	self->records = (PHashRecord *)realloc(records, sizeof(PHashRecord) * self->tableSize * 2);
	memcpy(self->records, other->records, sizeof(PHashRecord) * self->tableSize * 2);
}

/*	this is where our cuckoo acts :
	it kicks records out of nests until it finds an empty nest or gets tired
	returns the empty nest if found, or NULL if it is too tired 
*/
PHashRecord *PHash_cuckoo_(PHash *self, PHashRecord* thisRecord)
{
	unsigned int hash;
	PHashRecord* record;
	record = PHash_recordAt_(self, thisRecord->key);

	if (record != &self->nullRecord && NULL == record->key)
		return record;
	else
	{
		if (PHashKey_isEqual_(thisRecord->key, record->key))
		{
			return record;
		}
		else
		{
			int i;
			// to balance load
			if (self->balance)
			{
				self->balance = 0;

				hash = PHash_hash_more(self, PHash_hash(self, thisRecord->key));
				record = CHASH_RECORDS_AT_HASH_(self, 1, hash);
				if (NULL == record->key) 
					return record;
				else
					PHashRecord_swap(record, thisRecord);

				if (PHashKey_isEqual_(thisRecord->key, record->key))
					return record;
			}
			self->balance = 1;

			for (i=0; i<self->maxLoops; i++)
			{
				hash = PHash_hash(self, thisRecord->key);
				record = CHASH_RECORDS_AT_HASH_(self, 0, hash);
				if (NULL == record->key) 
					return record;
				else
					PHashRecord_swap(record, thisRecord);

				if (PHashKey_isEqual_(thisRecord->key, record->key))
					return record;

				hash = PHash_hash_more(self, PHash_hash(self, thisRecord->key));
				record = CHASH_RECORDS_AT_HASH_(self, 1, hash);
				if (NULL == record->key) 
					return record;
				else
					PHashRecord_swap(record, thisRecord);

				if (PHashKey_isEqual_(thisRecord->key, record->key))
					return record;
			}

			// the cuckoo is tired ^^
			return NULL;
		}
	}

	return NULL;
}

void PHash_grow(PHash *self)
{
	unsigned int oldTableSize = self->tableSize;
	PHashRecord* oldRecords = self->records;

	self->records = NULL;
	while (self->records == NULL)
	{
		unsigned int i;

		PHash_tableInit_(self, self->log2tableSize + 1);

		// enumerate old records

		i = 0;
		while (i < oldTableSize*2)
		{
			PHashRecord thisRecord = oldRecords[i];
			i++;
			
			if (thisRecord.key)
			{
				PHashRecord *record = PHash_cuckoo_(self, &thisRecord);
				if (!record) // collision
				{
					free(self->records);
					self->records = NULL;
					break; // grow & rehash
				}
				*record = thisRecord;
			}
		}
	}
	free(oldRecords);
}

void PHash_growWithRecord(PHash *self, PHashRecord* thisRecord)
{
	// put the value anywhere, PHash_grow will rehash
	unsigned int i,j;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);

		if (record != &self->nullRecord && NULL == record->key) 
		{
			*record = *thisRecord;
			self->numKeys ++;
			PHash_grow(self);
			return;
		}
	}

	// we can never be there
	return;
}


void *PHash_firstValue(PHash *self)
{
	self->index = 0;
	return PHash_nextValue(self);
}

void *PHash_nextValue(PHash *self)
{
	PHashRecord *record;
	
	while (self->index < self->tableSize*2)
	{
		record = self->records + self->index;
		self->index++;
		
		if (record->key) 
		{
			return record->value;
		}
	}
	
	return (void *)NULL;
}

void PHash_removeValue_(PHash *self, void *value)
{
	PHashRecord *record;
	int index = 0;
	
	while (index < self->tableSize*2)
	{
		record = self->records + index;
		index ++;
		
		if (record->key && record->value == value)
		{
			self->numKeys --;
			memset(record, 0, sizeof(PHashRecord));
			return;
		}
	}
}

void *PHash_firstKey(PHash *self)
{
	self->index = 0;
	return PHash_nextKey(self);
}

void *PHash_nextKey(PHash *self)
{
	PHashRecord *record;
	
	while (self->index < self->tableSize*2)
	{
		record = self->records + self->index;
		self->index ++;
		
		if (record->key) 
		{
			return record->key;
		}
	}
	
	return (void *)NULL;
}

unsigned int PHash_doCount(PHash *self)
{
	unsigned int i,j;
	unsigned int count = 0;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
		
		if (record->key) 
		{
			count++;
		}
	}
	
	return count;
}

PHashRecord *PHash_recordAtIndex_(PHash *self, unsigned int index)
{
	unsigned int i,j;
	unsigned int count = 0;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
		
		if (record->key) 
		{
			if (index == count) 
			{ 
				return record; 
			}
			count++;
		}
	}
	
	return (PHashRecord *)NULL;
}

void *PHash_keyAt_(PHash *self, unsigned int i)
{
	return PHash_recordAtIndex_(self, i)->key;
}

void *PHash_valueAt_(PHash *self, unsigned int i)
{
	return PHash_recordAtIndex_(self, i)->value;
}

int PHash_indexForValue_(PHash *self, void *v)
{
	unsigned int i,j;
	unsigned int count = 0;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);

		if (record->key)
		{
			if (record->value == v) 
			{ 
				return count; 
			}
			
			count++;
		}
	}
	
	return -1;
}

void *PHash_firstKeyForValue_(PHash *self, void *v)
{
	unsigned int i,j;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);

		if (record->key && record->value == v)
			return record->key; 
	}
	return NULL;
}

// --- enumeration -------------------------------------------------- 

void PHash_do_(PHash *self, PHashDoCallback *callback)
{
	unsigned int i, j;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
		
		if (record->key) 
		{ 
			(*callback)(record->value); 
		}
	}
}

void *PHash_detect_(PHash *self, PHashDetectCallback *callback)
{
	unsigned int i, j;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
		
		if (record->key) 
		{ 
			if ((*callback)(record->value))
			{
				return record->key;
			}
		}
	}
	
	return NULL;
}

void PHash_doOnKeys_(PHash *self, PHashDoCallback *callback)
{
	unsigned int i, j;
	
	for (j = 0; j < 2; j ++)
	for (i = 0; i < self->tableSize; i ++)
	{
		PHashRecord *record = CHASH_RECORDS_AT_(self, j, i);
		
		if (record->key) 
		{ 
			(*callback)(record->key); 
		}
	}
}
