
#ifdef IO_USE_VECTOROPS

#include <stdio.h>
#include <stdlib.h>
#include "Common.h"
#include <math.h>

typedef float  float32_t;
typedef double float64_t;

#ifdef __GNUC__

// see: http://ds9a.nl/gcc-simd/example.html

#define VEC_SIZE 4


typedef float v4sf __attribute__ ((mode(V4SF))); // vector of four single floats
union f4vector 
{
	v4sf vec;
	float32_t f[4];
};

typedef   int8_t     v_int8_t __attribute__ ((vector_size(VEC_SIZE)));
typedef   int16_t   v_int16_t __attribute__ ((vector_size(VEC_SIZE)));
typedef   int32_t   v_int32_t __attribute__ ((vector_size(VEC_SIZE)));
typedef   int64_t   v_int64_t __attribute__ ((vector_size(VEC_SIZE)));
typedef float32_t v_float32_t __attribute__ ((vector_size(VEC_SIZE)));
typedef float64_t v_float64_t __attribute__ ((vector_size(VEC_SIZE)));

typedef union { v_int8_t    vec; int8_t    v[VEC_SIZE]; } vint8_t;
typedef union { v_int16_t   vec; int16_t   v[VEC_SIZE]; } vint16_t;
typedef union { v_int32_t   vec; int32_t   v[VEC_SIZE]; } vint32_t;
typedef union { v_int64_t   vec; int64_t   v[VEC_SIZE]; } vint64_t;
typedef union { v_float32_t vec; float32_t v[VEC_SIZE]; } vfloat32_t;
typedef union { v_float64_t vec; float64_t v[VEC_SIZE]; } vfloat64_t;

#define VEC_DUALARG_OP(NAME, OP, TYPE1, TYPE2) \
void v ## TYPE1 ## _ ## NAME ## _v ## TYPE2 ## _size_(TYPE1 ##_t *aa, TYPE2 ##_t *bb, size_t size)\
{\
	size_t i = 0;\
	v ## TYPE1 ##_t *a = (v ## TYPE1 ##_t *)aa;\
	v ## TYPE2 ##_t *b = (v ## TYPE2 ##_t *)bb;\
	size_t max = (size / VEC_SIZE);\
	for(i = 0; i < max; i ++) { a[i].vec OP b[i].vec; }\
	i = i * VEC_SIZE;\
	\
	while (i < size)\
	{\
		aa[i] OP bb[i];\
		i ++;\
	}\
}

#define VEC_2ARG_FUNC(NAME, VFUNC, FUNC, TYPE1, TYPE2) \
void v ## TYPE1 ## _ ## NAME ## _v ## TYPE2 ## _size_(TYPE1 ##_t *aa, TYPE2 ##_t *bb, size_t size)\
{\
	size_t i = 0;\
	v ## TYPE1 ##_t *a = (v ## TYPE1 ##_t *)aa;\
	v ## TYPE2 ##_t *b = (v ## TYPE2 ##_t *)bb;\
	size_t max = (size / VEC_SIZE);\
	for(i = 0; i < max; i ++) { a[i].vec = VFUNC(a[i].vec, b[i].vec); }\
	i = i * VEC_SIZE;\
	\
	while (i < size)\
	{\
		aa[i] = FUNC(aa[i], bb[i]);\
		i ++;\
	}\
}

#define VEC_SINGLEARG_OP(NAME, VFUNC, CAST, NVFUNC, TYPE1) \
void v ## TYPE1 ## _ ## NAME (TYPE1 ##_t *aa, size_t size)\
{\
	size_t i = 0;\
	v ## TYPE1 ##_t *a = (v ## TYPE1 ##_t *)aa;\
	size_t max = (size / VEC_SIZE);\
	for(i = 0; i < max; i ++) { a[i] = VFUNC(a[i].vec); }\
	i = i * VEC_SIZE;\
	\
	while (i < size)\
	{\
		aa[i] = NVFUNC((CAST) aa[i]);\
		i ++;\
	}\
}

#else

#define VEC_DUALARG_OP(NAME, OP, TYPE1, TYPE2) \
void v ## TYPE1 ## _ ## NAME ## _v ## TYPE2 ## _size_(TYPE1 ##_t *aa, TYPE2 ##_t *bb, size_t size)\
{\
	size_t i = 0;\
	while (i < size)\
	{\
		aa[i] OP bb[i];\
		i ++;\
	}\
}

#endif

int vint8_equals_vint8_size_(int8_t *aa, int8_t *bb, size_t size)
{
	size_t i = 0;
	
#if defined(__i386__) && defined(__GNUC__)
	union f4vector *a = (union f4vector *)aa;
	union f4vector *b = (union f4vector *)bb;
	size_t max = (size / (VEC_SIZE * sizeof(float32_t)) );
	for(i = 0; i < max; i ++) 
	{ 
		if(__builtin_ia32_comieq(a[i].vec, b[i].vec) == 0) 
		{
			return 0;
		}
	}
	i = i * VEC_SIZE * sizeof(float32_t);
#endif
	
	while (i < size)
	{
		if(aa[i] != bb[i]) return 0;
		i ++;
	}
	
	return 1;
}


#define IGVOP(NAME, OP) \
VEC_DUALARG_OP(NAME, OP, int8,  int8); \
VEC_DUALARG_OP(NAME, OP, int8,  int16); \
VEC_DUALARG_OP(NAME, OP, int8,  int32); \
VEC_DUALARG_OP(NAME, OP, int16, int8); \
VEC_DUALARG_OP(NAME, OP, int16, int16); \
VEC_DUALARG_OP(NAME, OP, int16, int32); \
VEC_DUALARG_OP(NAME, OP, int32, int8); \
VEC_DUALARG_OP(NAME, OP, int32, int16); \
VEC_DUALARG_OP(NAME, OP, int32, int32); \
//VEC_DUALARG_OP(NAME, OP, int64, int64); 

#define FGVOP(NAME, OP) \
VEC_DUALARG_OP(NAME, OP, float32, float32); \
//VEC_DUALARG_OP(NAME, OP, float64, float64);

#endif
/*
IGVOP(add,      +=);
IGVOP(subtract, -=);
IGVOP(multiply, *=);
IGVOP(divide,   /=);
IGVOP(copy,      =);

IGVOP(bitwiseXor, ^=);
IGVOP(bitwiseOr,  |=);
IGVOP(bitwiseAnd, &=);
//IVOP(bitwiseNot, =~);

FGVOP(add,      +=);
FGVOP(subtract, -=);
FGVOP(multiply, *=);
FGVOP(divide,   /=);
FGVOP(copy,      =);
*/

/*
#include <time.h>

#define TEST_TYPE float32_t

int main(int argc, const char * argv[]) 
{
	size_t vsize = 40000;
	size_t iters = 10000;
	TEST_TYPE *aa = (TEST_TYPE *)io_calloc(1, vsize * sizeof(int));
	TEST_TYPE *bb = (TEST_TYPE *)io_calloc(1, vsize * sizeof(int));
	size_t i;
	float t1, t2, dt;
	
	for(i = 0; i < vsize; i ++) { aa[i] = 1; }
	for(i = 0; i < vsize; i ++) { bb[i] = 1; }
	
	 t1 = clock();
	 for(i = 0; i < iters; i ++)
	 {
		 vfloat32_multiply_vfloat32_size_(aa, bb, vsize);
		 vfloat32_multiply_vfloat32_size_(aa, bb, vsize);
	 }
	 t2 = clock();
	 
	 dt = (t2 - t1) / (float)CLOCKS_PER_SEC;
	 printf("%.2f seconds to run\n", dt);
	 printf("%.3f GFLOPS\n", (iters * vsize * 2.0 / dt)/1000000000.0);

	//for(i = 0; i < vsize; i ++) { printf("%i ", aa[i]); }
	//printf("\n");
	
	//printf("eq %i\n", int32_t_vector_equal(aa, bb, 6));
	t1 = clock();
	for (i = 0; i < 100000; i ++)
	{
		int8_t *aa = (int8_t *)io_calloc(1, 401);
		int8_t *bb = (int8_t *)io_calloc(1, 401);
		aa[1] = 0;
		aa[400] = 0;
		int r = vint8_equals_vint8_size_(aa, bb, 401);
		//printf("equals = %i\n", r);
	}
	t2 = clock();
	
	dt = (t2 - t1) / (float)CLOCKS_PER_SEC;
	printf("%.2f seconds to run\n", dt);
	return 0;
}
*/
