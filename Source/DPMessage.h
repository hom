//
//  DPMessage.h
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 09/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. Neither the name of Ofri Wolfus nor the names of his contributors
//  may be used to endorse or promote products derived from this software
//  without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
//  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#if defined(__HOM_FRAMEWORK__)
#import <HigherOrderMessaging/DPObjCRuntime.h>
#elif defined(__UITESTINGKIT__)
#import <UITestingKit/DPObjCRuntime.h>
#else
#import "DPObjCRuntime.h"
#endif

#if __OBJC2__
#import <objc/runtime.h>
#import <objc/message.h>
#else
#import <objc/objc-class.h>
#import <objc/objc-runtime.h>
#endif

#include <alloca.h>


@class NSString;
@protocol NSCopying;

#define DP_MAX_FRAME_SIZE 1024

// This struct is private
struct _dp_message_content {
	SEL _sel;
	unsigned int _frameSize;
	char _frame[DP_MAX_FRAME_SIZE];
};


// Warning: In the current implementation it's undefined what happens
// if the arguments frame is larger than sizeof(char[1024]).
@interface DPMessage {
	Class isa;
	
	@private
	struct _dp_message_content content;
}

+ (id)messageWithSelector:(SEL)sel frame:(void *)frame;

- (SEL)selector;
- (marg_list)arguments;
- (unsigned int)sizeOfArguments;

@end


@interface DPMessage (Extensions)

/*
 * Returns whether the receiver assumes to return a struct or not.
 *
 * If you know you're goign to send the receiver to an object that'll
 * return a struct and the result of this method is NO, you should
 * allocate a new arguments list with a pointer to a memory for the returned structure
 * at the beginning of it. Then copy the list of the receiver after the pointer of
 * your new list. On the other hand, if you know your object is not going to return a
 * struct, and the result of this method is YES, you must allocate a new
 * arguments list that starts with a pointer to your receiver.
 *
 * NOTE: The result of this method is not always accurate. Depending on the
 * architecture and its function calling ABI, methods returning small structs
 * may not be considered to return structs.
 */
- (BOOL)returnsStruct;

// This is the same as sel_getNumberOfArguments([msg selector])
- (unsigned)numberOfArguments;

// For non object return values you must use one of the objc_msgSendv() functions directly.
- (id)sendTo:(id)receiver;

+ (Class)class;
- (Class)class;

- (NSString *)description;
- (BOOL)respondsToSelector:(SEL)aSelector;

/*
 * Returns an arguments frame that can be passed to __builtin_apply().
 * The size of the frame (passed as the third argument to __builtin_apply())
 * is the same as the size returned by -sizeOfArguments method.
 * A new frame is dynamically allocated using malloc() with each invocation
 * of this method and you are responsible for releasing it.
 *
 * It is not documented anywhere, but currently simply passing marg_list to
 * __builtin_return() works (tested on G4 with Max OS X 10.4.9).
 *
 * NOTE: This method only works for methods returning integer values.
 * For other return type the result is undefined and invoking this method
 * will probably cause a crash.
 */
- (void *)gccArgumentsFrame;

// sizeOfArguments is *NOT* the size of the frame.
// Use this method to get the size of the frame.
- (unsigned)realFrameSize;

@end


@interface DPMessage (MemoryManagement) <NSCopying>

// Returns a copy of the receiver allocated on the heap
// (a normal instance that's ref counted).
- (id)copyWithZone:(NSZone *)zone;
- (id)copy;

// retain/release/autorelease have no effect on messages
// returned by the MSG() macro.
// Affected messages are those returned by -copyWithZone: or -copy
- (id)retain;
- (void)release;
- (id)autorelease;

@end


// Allocates and returns an instance on stack
#define DPAllocateStackObject(cls, extraBytes) \
({ id __obj = (id)alloca(((size_t)((Class)cls)->instance_size) + extraBytes); \
__obj->isa = cls; __obj; })

// Private
DP_EXTERN Class _dp_uninitialized_msg_cls;
DP_EXTERN Class _dp_msg_cls;
DP_EXTERN_INLINE id _dp_getCachedMessage(void);

/*
 * Use this macro to create DPMessage instance.
 * Currently, this (and MSGV) is the only way to be passed as an argument
 * to a method or function. If you wish to stoe the returned instance in
 * a variable, send it a -copy message and use the result instead.
 * The instance returned from the -copy method is ref-counted like normal
 * objects and follows the rules of the copy method.
 *
 * Example: NSArray *results = [myArray select:MSG(hasPrefix:@"a")];
 */
#define MSG(X...) \
({ id __msg = _dp_getCachedMessage(); Class __cls = __msg != nil ? __msg->isa : _dp_msg_cls;\
if (__builtin_expect(__msg == nil, 0)) __msg = DPAllocateStackObject(_dp_uninitialized_msg_cls, 0); \
__msg->isa = _dp_uninitialized_msg_cls; [__msg X]; __msg->isa = __cls; __msg; })

/*
 * An alternative to MSG(). This macro takes a selector followed by the arguments
 * of the message, and returns a matching DPMessage instance.
 * The returned instance is a subject to the same rules and restrictions of
 * instances returned by MSG().
 *
 * Example: NSArray *results = [myArray select:MSGV(@selector(hasPrefix:), @"a")];
 */
#define MSGV(sel...) \
({ id __msg = _dp_getCachedMessage(); Class __cls = __msg != nil ? __msg->isa : _dp_msg_cls;\
	if (__builtin_expect(__msg == nil, 0)) __msg = DPAllocateStackObject(_dp_uninitialized_msg_cls, 0); \
		__msg->isa = _dp_uninitialized_msg_cls; objc_msgSend(__msg, sel); __msg->isa = __cls; __msg; })

/*
 * Returns the maximum (as returned by method_getSizeOfArguments) of a
 * given selector. In order to be efficient, an internal cache is used
 * to cache all selectors.
 *
 * The implementation will catch classes being added at runtime, but will not
 * notice methods being dynamically added and classes unregistering (actually,
 * the implementation uses the total amount of registered classes as in indication
 * the cache needs to be rebuilt, so it'll not catch only the case of X classes
 * being registered while X classes unregister [which will result in the same
 * total of registered classes]).
 * In these edge cases, use dp_flushArgSizeCache() to force the cache to be
 * rebuilt the next time the function is invoked.
 *
 * This function is used internally by DPMessage so if it returns wrong results
 * so will DPMessage.
 */
DP_EXTERN unsigned int dp_maxArgSizeForSelector(SEL sel);
DP_EXTERN void dp_flushArgSizeCache(void);

DP_EXTERN unsigned dp_getCacheSize(void);

/*
 * When defined to 1, no attempts to auto-rebuild the cache based on the class
 * count are made.
 * Instead, the implementation listens to NSBundleDidLoadNotification and
 * flushes the cache when the notification arrives.
 * If you're going to add classes in other ways than NSBundle and still want
 * the count-based implementation, define DP_NSBUNEL_DETECTION_ONLY to 0.
 */
#ifndef DP_NSBUNEL_DETECTION_ONLY
#define DP_NSBUNEL_DETECTION_ONLY 1
#endif
