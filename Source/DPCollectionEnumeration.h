//
//  DPCollectionEnumeration.h
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 11/05/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. Neither the name of Ofri Wolfus nor the names of his contributors
//  may be used to endorse or promote products derived from this software
//  without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
//  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <Foundation/Foundation.h>

#if defined(__HOM_FRAMEWORK__)
#import <HigherOrderMessaging/DPObjCRuntime.h>
#elif defined(__UITESTINGKIT__)
#import <UITestingKit/DPObjCRuntime.h>
#else
#import "DPObjCRuntime.h"	// For DP_EXTERN
#endif

#if defined(__HOM_FRAMEWORK__)
#import <HigherOrderMessaging/DPMessage.h>
#else
#import "DPMessage.h"
#endif


typedef struct DPEnumerationState DPEnumerationState;

/*
 * A callback function invoked before an enumeration is completed.
 * This function is responsible of releasing the info and items field,
 * and perform any other needed cleanups.
 */
typedef void (*DPEnumeratioStateReleaseCallBack) (DPEnumerationState *state);

struct DPEnumerationState {
	// The state of the enumeration, usually an index.
	// Each call to enumerateWithState:objects:count: should
	// read the state and update it to the new one.
	unsigned long state;
	// A buffer to the items of the collection. By default,
	// the buffer points to the same buffer passed by
	// enumerateWithState:objects:count:. If the collection
	// doesn't use that buffer, it is responsible for setting
	// the items poiner to its own buffer. The buffer should
	// be released by the release callback function.
	id *items;
	// A private info used by the collection. The collection
	// free to do whatever it wants with it.
	void *info;
	// A release callback for the info field and the objects
	// buffer if the collection created it. The first call (when
	// state == 0) to enumerateWithState:objects:count: is
	// responsible for setting the release callback if needed.
	DPEnumeratioStateReleaseCallBack release;
};

/*!
 * @abstract A protocol defining a collection-independent
 * fast enumeration.
 */
@protocol DPEnumeration

/*!
 * @abstract Given a state and a buffer of objects, this method
 * returns the next bunch of objects from the receiver.
 *
 * @discussion This method is repeatedly called during enumeration
 * in order to get the next bunch of objects. The collection (the
 * receiver) is responsible for updating the state of the enumeration.
 * 
 * @param state A pointer to a state structure holding the info of
 * the enumeration. See above for more info of its fields.
 *
 * @param buff A buffer for placing the objects from the collection in.
 * A buffer will must always be provided with this method, but the
 * collection may choose not to use it and use its own buffer. In this
 * case the collection is responsible for setting the <code>items</code>
 * field of the enmeration state to its own buffer.
 *
 * @param buffLen The amount of objects that fit in the buffer.
 *
 * @result The number of objects returned by the collection.
 */
- (unsigned)enumerateWithState:(DPEnumerationState *)state
					   objects:(id *)buff
						 count:(unsigned)buffLen;

@end


/*!
 * @abstract A special enumerator designed for the <code>each</code> method.
 */
@interface DPEnumerator : NSObject {
	void *_reserved;
}

/*!
 * @abstract Returns an enumerator for a given objects buffer.
 *
 * @param objects A C array of objects.
 * @param count The size of the objects array.
 * @param flag A flag indicating whether the enumerator should
 * free the objects array when deallocating or not.
 */
+ (id)enumeratorWithObjects:(id *)objects
					  count:(unsigned)count
			   freeWhenDone:(BOOL)flag;

/*!
 * @abstract Returns a lightweight enumerator for a given array.
 */
+ (id)enumeratorForArray:(NSArray *)arr;

/*!
 * @abstract Returns an enumerator for a given collection.
 */
+ (id)enumeratorForCollection:(id <DPEnumeration>)collection;

- (id)nextObject;
- (NSArray *)allObjects;

// The following methods are not supported at this time
- (BOOL)isValid;
- (unsigned)count;
- (void)reset;

@end

/*!
 * @abstract A function that's repeatedly called during enumeration to append
 * objects to the results collection.
 *
 * @param resultsCollection The collection to append the objects to.
 * @param state The state of the enumeration.
 * @param objects An array of objects to append to the collection.
 * @param count The number of objects in the objects array.
 */
typedef void (*DPEnumerationAppendResultsCallBack) (id resultsCollection,
													DPEnumerationState *state,
													id *objects,
													unsigned count);

/*
 * Generic implementations of the collect:, select/rejectWhere:
 * and findObjectWhere: methods. Use them only if you're adding
 * support for a new collection.
 */
DP_EXTERN void DPCollectObjects(id <DPEnumeration> collection,
								id resultsCollection,
								DPMessage *message,
								DPEnumerationAppendResultsCallBack callback);

DP_EXTERN void DPSelectObjects(id <DPEnumeration> collection,
							   id resultsCollection,
							   DPMessage **messages,
							   unsigned messageCount,
							   BOOL returnValue,
							   DPEnumerationAppendResultsCallBack callback);

DP_EXTERN id DPFindObject(id <DPEnumeration> collection,
							DPMessage **messages,
							unsigned messageCount,
							BOOL returnValue);

// Returns whether a given pointer is an enumerated argument or not.
// It does not involve any message sending or dereferencing the pointer,
// so it's safe for use with any "random" pointer.
DP_EXTERN_INLINE BOOL DPIsEnumeratedArgument(void *ptr);


/*!
 * @abstract Defines the HOM based enumeration methods.
 */
@protocol DPCollectionEnumeration <DPEnumeration>

/*!
 * @abstract Sends the passed message to all objects of the
 * receiver and returns the results.
 *
 * @discussion If an object doesn't return an object, or it
 * returns <code>nil</code>, the result is undefined.
 * This method supports enumerated arguments returned by the
 * <code>each</code> method.
 */
- (id)collect:(DPMessage *)argumentMessage;

/*!
 * @abstract Returns all objects of the receiver that returned
 * <code>YES</code> for the last message.
 *
 * @discussion The messages list must be NULL/nil terminated.
 * This method supports enumerated arguments returned by the
 * <code>each</code> method.
 */
- (id)selectWhere:(DPMessage *)firstMessage, ...;

/*!
 * @abstract Returns all objects of the receiver that returned
 * <code>NO</code> for the last message.
 *
 * @discussion The messages list must be NULL/nil terminated.
 * This method supports enumerated arguments returned by the
 * <code>each</code> method.
 */
- (id)rejectWhere:(DPMessage *)firstMessage, ...;

/*!
 * @abstract Returns the first object of the receiver that returned
 * <code>YES</code> for the last message.
 *
 * @discussion The messages list must be NULL/nil terminated.
 * This method supports enumerated arguments returned by the
 * <code>each</code> method.
 */
- (id)findObjectWhere:(DPMessage *)firstMessage, ...;

/*!
 * @abstract Returns an enumerated argument that can be passed
 * to one of the above methods.
 *
 * @discussion It's also possible to message the returned object
 * directly, in which case it'll forward the message to all of
 * its objects.
 * For more info, please refer to the HOM paper at
 * http://www.metaobject.com/papers/Higher_Order_Messaging_OOPSLA_2005.pdf
 */
- (id)each;

@end

@interface NSArray (DPCollectionEnumeration) <DPCollectionEnumeration>
@end

@interface NSSet (DPCollectionEnumeration) <DPCollectionEnumeration>
@end

@interface NSDictionary (DPCollectionEnumeration) <DPCollectionEnumeration>
@end


@interface NSObject (DPHOMGoodies)

/*!
 * @abstract Makes the receiver receive the passed message.
 * @discussion This method accepts enumerated arguments.
 * This is the equivalent of the <code>do</code> method
 * described in Marcel's HOM paper.
 */
- (id)collect:(DPMessage *)msg;

/*!
 * @abstract Makes the receiver receive the passed message.
 * @discussion The receiver will receive the passed message,
 * repeating as many times needed for enumerated arguments.
 * Upon completion, returns the result of the message or the
 * result of the last message sent if there were any enumerated
 * arguments.
 */
- (id)receive:(DPMessage *)msg;

/*!
 * @abstract Makes the receiver receive the passed message
 * only if it responds to it.
 *
 * @discussion Only object return values are supported.
 * For other return types the behaviour is undefined.
 * NOTE: This method does *NOT* support enumerated arguments.
 */
- (id)ifResponds:(DPMessage *)msg;

@end


/*
 * And when HOM fails to do what you need, foreach() is always handy :)
 * In ObjC 1 it'll just send a -objectEnumerator to the collection
 * while in ObjC 2 it translates to the native for(in) syntax.
 *
 * Example: foreach (NSString *str, myArr) { [str doSomething]; }
 */
#if !__OBJC2__

/*
 Code taken from CocoaDev. See http://www.cocoadev.com/index.pl?ForallMacro for more details.
 Modified by Ofri Wolfus.
 */
#define foreach(element, collection...) \
for(id __foreach_macro_element = nil, __foreach_macro_enumerator = [collection objectEnumerator]; \
	__foreach_macro_element = !__foreach_macro_element ? [__foreach_macro_enumerator nextObject] : nil;) \
		for (element = __foreach_macro_element; __foreach_macro_element; __foreach_macro_element = nil)

#else

// For ObjC 2 we just use the built in for (in) syntax
#define foreach(obj, collection...) for(obj in collection)

#endif
