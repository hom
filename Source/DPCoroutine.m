//
//  DPCoroutine.m
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 01/08/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import "DPCoroutine.h"
#include <libkern/OSAtomic.h>


@interface DPCoroutineStack (private)

- (DPCoroutine *)mainCoro;
- (DPCoroutine *)newCoroutineWithMessage:(DPMessage *)msg target:(id)t;
- (BOOL)nextCoro;
- (void)_setMain;

@end

@interface DPCoroutine (private)

+ (void)setCurrentCoroutine:(id)coro;

- (void)setMain;
- (BOOL)isMain;

- (BOOL)continue;
- (void)return;
- (void)resetWithMessage:(DPMessage *)msg;
- (void)setTarget:(id)target;

- (BOOL)isActive;

- (void)start;
- (Coro *)_coro;
- (void)setParent:(DPCoroutine *)co;

@end

@implementation DPCoroutine

#define DPCURRENT_COROUTINE_KEY	@"DPCurrentCoroutine"

+ (id)currentCoroutine {
	return [[[NSThread currentThread] threadDictionary] objectForKey:DPCURRENT_COROUTINE_KEY];
}

+ (void)setCurrentCoroutine:(id)coro {
	if (coro)
		[[[NSThread currentThread] threadDictionary] setObject:coro forKey:DPCURRENT_COROUTINE_KEY];
	else
		[[[NSThread currentThread] threadDictionary] removeObjectForKey:DPCURRENT_COROUTINE_KEY];
}

- (id)init {
	if ((self = [super init])) {
		_refCount = 1;
		_coro = Coro_new();
		isActive = NO;
		message = nil;
	}
	
	return self;
}

- (id)initWithMessage:(DPMessage *)msg target:(id)t {
	// Can't initialize without a message or a targe
	if (!msg || !t) {
		[self release];
		return nil;
	}
	
	if ((self = [self init])) {
		message = [msg copy];
		target = [t retain];
	}
	
	return self;
}

- (void)dealloc {
	[target release]; target = nil;
	[message release]; message = nil;
	Coro_free(_coro);
	[super dealloc];
}

- (id)retain {
	OSAtomicIncrement32Barrier(&_refCount);
	return self;
}

- (void)release {
	if (OSAtomicDecrement32Barrier(&_refCount) == 0)
		[self dealloc];
}

- (unsigned)retainCount {
	OSMemoryBarrier();
	return (unsigned)_refCount;
}

- (void)setMain {
	if (!_coro->isMain) {
		Coro_initializeMainCoro(_coro);
		isActive = YES;
	}
}

- (BOOL)isMain {
	return _coro->isMain;
}

- (void)resetWithMessage:(DPMessage *)msg {
	//Coro_clear(_coro);
	[message release];
	message = [msg retain];
}

- (void)setTarget:(id)t {
	if (target != t) {
		[target release];
		target = [t retain];
	}
}

- (void)yield {
	//Coro_yield();
	if (__builtin_expect(Coro_stackSpaceAlmostGone(_coro), 0)) {
		NSLog(@"Coroutine %@'s stack is almost full. Allocating some more in hope not to crash.", self);
		Coro_setStackSize_(_coro, Coro_stackSize(_coro) * 2);
	}
	
	Coro_switchTo_(_coro, [parent _coro]);
}

- (void)return {
	isActive = NO;
	//Coro_end();
	[self yield];
}

- (BOOL)isActive {
	return isActive;
}

static void _DPCoroEntry(DPCoroutine *coro) {
	coro->isActive = YES;
	[coro->message sendTo:coro->target];
	[coro return];
}

- (Coro *)_coro {
	return _coro;
}

- (void)start {
	NSMutableDictionary *dict = [[NSThread currentThread] threadDictionary];
	[dict setObject:self forKey:DPCURRENT_COROUTINE_KEY];
	Coro_startCoro_([parent _coro], _coro, self, (void *)_DPCoroEntry);
	[dict setObject:parent forKey:DPCURRENT_COROUTINE_KEY];
}

- (BOOL)continue {
	if (isActive) {
		NSMutableDictionary *dict = [[NSThread currentThread] threadDictionary];
		
		[dict setObject:self forKey:DPCURRENT_COROUTINE_KEY];
		Coro_switchTo_([parent _coro], _coro);
		[dict setObject:parent forKey:DPCURRENT_COROUTINE_KEY];
		return isActive;
	}
	
	return NO;
}

- (void)setParent:(DPCoroutine *)co {
	parent = co;
}

@end

@implementation DPCoroutineStack

static DPCoroutineStack *_mainStack = nil;

static void __attribute__((constructor)) _DPSetupMainCoroStack(void) {
	_mainStack = [[DPCoroutineStack alloc] init];
	[DPCoroutineStack setCurrentStack:_mainStack];
	[[NSRunLoop currentRunLoop] addSource:_mainStack
								  forMode:NSDefaultRunLoopMode];
}

+ (id)mainStack {
	return _mainStack;
}

+ (id)currentStack {
	return [[[NSThread currentThread] threadDictionary] objectForKey:@"DPCoroutineStack"];
}

+ (void)setCurrentStack:(id)stack {
	if (stack)
		[[[NSThread currentThread] threadDictionary] setObject:stack
														forKey:@"DPCoroutineStack"];
	else
		[[[NSThread currentThread] threadDictionary] removeObjectForKey:@"DPCoroutineStack"];
}

- (id)init {
	if ((self = [super init])) {
		mainCoros = [[NSMutableDictionary alloc] init];
		mainCorosLock = OS_SPINLOCK_INIT;
		stack = [[DPQueue alloc] init];
		unusedCoroutines = [[DPQueue alloc] init];
		activeRoutines = 0;
	}
	
	return self;
}

- (void)dealloc {
	[unusedCoroutines release]; unusedCoroutines = nil;
	[stack release]; stack = nil;
	[mainCoros release]; mainCoros = nil;
	[super dealloc];
}

- (DPCoroutine *)newCoroutineWithMessage:(DPMessage *)msg target:(id)target {
	DPCoroutine *coro = [unusedCoroutines dequeue];
	
	if (coro) {
		[coro resetWithMessage:msg];
		[coro setTarget:target];
	} else {
		coro = [DPCoroutine new];
	}
	
	return [coro autorelease];
}

- (void)addCoro:(DPCoroutine *)coro {
	[stack enqueue:coro];
	[coro setParent:[self mainCoro]];
	[self signal];
}

- (BOOL)nextCoro {
	DPCoroutine *coro;
	
	OSAtomicIncrement32Barrier(&activeRoutines);
	coro = [stack dequeue];
	
	if (coro) {
		BOOL shouldContinue;
		
		[coro retain];
		[self retain];
		[self signal];
		
		if ([coro isActive]) {
			shouldContinue = [coro continue];
		} else {
			[coro start];
			shouldContinue = [coro isActive];
		}
		
		// There's no active coroutine
		[DPCoroutine setCurrentCoroutine:nil];
		
		// Clean up
		shouldContinue ? [stack enqueue:coro] : [unusedCoroutines enqueue:coro];
		[coro release];
		[self release];
	}
	
	OSAtomicDecrement32Barrier(&activeRoutines);
	
	return coro != nil;
}

- (DPCoroutine *)mainCoro {
	DPThreadID *trID = [DPThreadID currentIdentifier];
	OSSpinLockLock(&mainCorosLock);
	DPCoroutine *coro = [mainCoros objectForKey:trID];
	
	if (!coro) {
		coro = [[DPCoroutine alloc] init];
		[coro setMain];
		[mainCoros setObject:coro forKey:trID];
		[coro release];
	}
	OSSpinLockUnlock(&mainCorosLock);
	return coro;
}

- (void)empty {
	while ([self hasWork])
		[self nextCoro];
}

- (void)fire {
	[self nextCoro];
}

- (BOOL)hasWork {
	return [stack count] + activeRoutines > 0;
}

@end

@implementation NSObject (DPCoroutine)

- (void)coroutine:(DPMessage *)msg {
	DPCoroutine *coro = [[DPCoroutine alloc] initWithMessage:msg
													  target:self];
	[[DPCoroutineStack currentStack] addCoro:coro];
	[coro release];
}

@end
