//
//  DPCoroutine.h
//  HigherOrderMessaging
//
//  Created by Ofri Wolfus on 01/08/07.
//  Copyright 2007 Ofri Wolfus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPMessage.h"
#import "DPMultiThreading.h"
#import "Coro.h"


@interface DPCoroutine : NSObject {
	int32_t _refCount;
	Coro *_coro;
	BOOL isActive;
	DPMessage *message;
	id target;
	DPCoroutine *parent;
}

/*!
 * @abstract Returns the currently active coroutine in the current thread
 * or nil if no coroutine is active.
 */
+ (id)currentCoroutine;


/*!
 * @abstract Creates and returns a new coroutine.
 *
 * @discussion The returned instance must be added to a <code>DPCoroutineStack</code>
 * in order to start execution.
 *
 * @param msg The message which its method will be executed as the coroutine's body.
 * @param target The object which will receiver the message.
 *
 * @result An initialized DPCoroutine instance or <code>nil</code> if either <code>msg</code>
 * or </code>target</code> is <code>nil</code>.
 */
- (id)initWithMessage:(DPMessage *)msg target:(id)target;

/*!
 * @abstract Stops the execution of the receiver and passes control to the coroutine's stack.
 *
 * @discussion This method causes the receiver to save its current execution state and return
 * control to its stack. The receiver's coroutine stack then schedules the receiver to continue
 * execution later, and executes other coroutines until then.
 * Don't use this method unless you have to. Use the <code>DPCoroutineYield()</code> macro instead.
 */
- (void)yield;

@end


@interface DPCoroutineStack : DPRunLoopSource {
	OSSpinLock mainCorosLock;
	NSMutableDictionary *mainCoros;
	DPQueue *stack;
	DPQueue *unusedCoroutines;
	int32_t activeRoutines;
}

/*!
 * @abstract Returns the current active stack in the current thread.
 */
+ (id)currentStack;

/*!
 * @abstract Sets the current stack.
 */
+ (void)setCurrentStack:(id)stack;

/*!
 * @abstract Returns the default coroutine stack of the main thread.
 */
+ (id)mainStack;


/*!
 * @abstract Adds a coroutine to the receiver's queue, which will be
 * executed later.
 */
- (void)addCoro:(DPCoroutine *)coro;

/*!
 * @abstract Executes the next coroutine in the queue.
 * @discussion You rarely, if ever, need to invoke this method directly.
 *
 * @result <code>YES</code> if the receiver processed a coroutine, <code>NO</code> otherwise.
 */
- (BOOL)nextCoro;

/*!
 * @abstract Returns whether the receiver has coroutines to process or not.
 * @discussion Due to thread safety issues, this method may not always be accurate
 * and might return <code>YES</code> even if the receiver has no work.
 */
- (BOOL)hasWork;

/*!
 * @abstract Makes the receiver process all its coroutines until they complete.
 *
 * @discussion This method starts a tight loop that exists only after all coroutines returned.
 * Don't invoke this method unless you're absolutely sure you need it.
 */
- (void)empty;

@end


@interface NSObject (DPCoroutine)

/*!
 * @abstract Creates a new coroutine with a given message and adds it to the current coroutine stack.
 *
 * @discussion This method does nothing if there's no active coroutine stack in the current thread.
 * A stack is automatically created for you in the main thread and added to the default runloop mode.
 * For threads you create yourself you must set up a stack by hand like this:
 *
 <code>
 - (void)startThreadMethod {
	 NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	 DPCoroutineStack *stack = [[DPCoroutineStack alloc] init];
	 
	 [DPCoroutineStack setCurrentStack:stack];
	 [[NSRunLoop currentRunLoop] addSource:stack forMode:NSDefaultRunLoopMode];
	 [[NSRunLoop currentRunLoop] run];
	 
	 [stack release];
	 [pool release];
 }
 </code>
 */
- (void)coroutine:(DPMessage *)msg;

@end

#ifndef DPCoroutineYield

/*!
 * @abstract Stops execution of the current coroutine and passes control to another coroutine.
 */
#define DPCoroutineYield() [[DPCoroutine currentCoroutine] yield]
#endif
